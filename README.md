# Dependencies
Profile (https://www.drupal.org/project/profile)

# Licenses
Icons made by https://www.flaticon.com/authors/those-icons (Those Icons)
https://www.flaticon.com/
Licensed by http://creativecommons.org/licenses/by/3.0/
