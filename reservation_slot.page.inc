<?php

/**
 * @file
 * Contains reservation_slot.page.inc.
 *
 * Page callback for Reservation slot entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Reservation slot templates.
 *
 * Default template: reservation_slot.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_reservation_slot(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
