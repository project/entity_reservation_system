<?php

/**
 * @file
 * Contains reservation_unit.page.inc.
 *
 * Page callback for Reservation unit entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Reservation unit templates.
 *
 * Default template: reservation_unit.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_reservation_unit(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
