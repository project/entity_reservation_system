<?php

namespace Drupal\entity_reservation_system_order\EventSubscriber;

use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\entity_reservation_system\ReservationsInterface;

/**
 * Sends an email when the order transitions to Fulfillment.
 */
class EntityReservationSystemOrderEventSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a new OrderFulfillmentSubscriber object.
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.place.post_transition' => ['changeReservationStatus', 0],
    ];
    return $events;
  }

  /**
   * Sends the email.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function changeReservationStatus(WorkflowTransitionEvent $event) {
    // Create the email.
    $order = $event->getEntity();
    $reservation = $order->reservation->entity;
    if ($reservation) {
      $config = $reservation->getBaseConfig();
      if (empty($config['config']['validation_is_required'])) {
        $reservation->setReservationStatus(ReservationsInterface::RESERVATION_STATUS_CONFIRMED);
        $reservation->status = 1;
        $reservation->save();
      }
      else {
        $reservation->setReservationStatus(ReservationsInterface::RESERVATION_STATUS_PENDING);
        $reservation->status = 1;
        $reservation->save();
      }
    }

  }

}
