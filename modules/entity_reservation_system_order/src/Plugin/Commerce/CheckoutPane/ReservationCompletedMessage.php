<?php

namespace Drupal\entity_reservation_system_order\Plugin\Commerce\CheckoutPane;

use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Utility\Token;
use Drupal\Core\Render\RendererInterface;

/**
 * Provides the completion message pane.
 *
 * @CommerceCheckoutPane(
 *   id = "reservation_completed_message",
 *   label = @Translation("Reservation completed message"),
 *   default_step = "complete",
 * )
 */
class ReservationCompletedMessage extends CheckoutPaneBase implements ContainerFactoryPluginInterface {

  /**
   * The token replacement instance.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $tokenService;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new CheckoutPaneBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
   *   The parent checkout flow.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Utility\Token $token_service
   *   The token replacement instance.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, Token $token_service, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);
    $this->tokenService = $token_service;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('token'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'completed_message' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationSummary() {
    if (!empty($this->configuration['completed_message']['value'])) {
      $summary = $this->t('Use custom message: Yes');
    }
    else {
      $summary = $this->t('Use custom message: No');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['completed_message'] = [
      '#type' => 'text_format',
      '#format' => !empty($this->configuration['completed_message']['format']) ? $this->configuration['completed_message']['format'] : 'basic_html',
      '#title' => $this->t('Completed message'),
      '#default_value' => !empty($this->configuration['completed_message']['value']) ? $this->configuration['completed_message']['value'] : NULL,
    ];

    $form['tokens'] = \Drupal::service('token.tree_builder')->buildRenderable(['commerce_order', 'reservation_slot']);
    $form['tokens']['#suffix'] = $this->t('The original entity tokens are available but are not know because them are dinamically assigned.');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['completed_message'] = $values['completed_message'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form['#theme'] = 'reservation_order_complete_message';
    if (!empty($this->configuration['completed_message']['value'])) {
      $reserved_entity = $this->entityTypeManager->getStorage($this->order->reservation->entity->getHostEntityType())->load($this->order->reservation->entity->getHostEntityId());
      $variables = [
        'commerce_order' => $this->order,
        'reservation' => $this->order->reservation->entity,
        'reserved_entity' => $reserved_entity,
      ];
      $langcode = $this->order->language();
      $token_options = ['langcode' => $langcode->getId(), 'clear' => TRUE];
      $value = $this->tokenService->replace($this->configuration['completed_message']['value'], $variables, $token_options);
      $pane_form['#completed_message'] = [
        '#type' => 'processed_text',
        '#text' => $value,
        '#format' => $this->configuration['completed_message']['format'],
      ];
    }
    $pane_form['#return_button'] = [
      '#type' => 'link',
      '#title' => $this->t('Return to reserved entity'),
      '#url' => $reserved_entity->toUrl(),
    ];
    $pane_form['#order_entity'] = $this->order;
    $pane_form['#reservation_entity'] = $this->order->reservation->entity;

    return $pane_form;
  }

}
