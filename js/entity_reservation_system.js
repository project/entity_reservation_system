/**
 * @file
 * Your custom code into javascript behaviour.
 */

(function ($, Drupal, settings) {

  Drupal.behaviors.datepickerInit = {
    attach: function (context, settings) {
      var forms = drupalSettings.entity_reservation_system.forms || [];
      forms.forEach(function (item) {
        var minDate = 0;
        var firstDay = 0;
        if (item.allow_all_calendar) {
          minDate = "-1y";
        }
        if (item.first_day) {
          firstDay = item.first_day;
        }
        $('.' + item['wrapper_datepicker']).once('reservation-datepicker').datepicker({
          changeMonth: true,
          changeYear: true,
          minDate: minDate,
          firstDay: firstDay,
          dateFormat: 'yy-mm-dd',
          altField : '.' + item['wrapper_datevalue'],
          defaultDate: $('.' + item['wrapper_datevalue']).val(),
          onSelect: function () {
            $('.' + item['wrapper_datevalue']).change();
          }
        });

        var wrapper_units = '.' + item.wrapper_units;
        if ($('.form-item-' + item.units_field_name + '-all', wrapper_units).length) {
          $('.form-item-' + item.units_field_name + '-all input.form-checkbox', wrapper_units).once('select-all-units').click(function () {
            $(this, wrapper_units).closest('.form-checkboxes').find('input.form-checkbox').prop('checked', $(this).prop('checked'));
          });
          $('.form-type-checkbox:not(.form-item-' + item.units_field_name + '-all) input', wrapper_units).once('unselect-all-units').click(function () {
            $(this, wrapper_units).closest('.form-checkboxes').find('.form-item-' + item.units_field_name + '-all input').prop('checked', false);
          });
        }
        item.incompatible_units.forEach(function (incompatible) {
          if ($('.form-item-' + item.units_field_name + '-' + incompatible, wrapper_units).length) {
            $('.form-item-' + item.units_field_name + '-' + incompatible + ' input.form-checkbox', wrapper_units).once('select-' + incompatible + '-units').click(function () {
              $(this, wrapper_units).closest('.form-checkboxes').find('.form-type-checkbox:not(.form-item-' + item.units_field_name + '-' + incompatible + ') input.form-checkbox').prop('checked', false);
            });
            $('.form-type-checkbox:not(.form-item-' + item.units_field_name + '-' + incompatible + ') input', wrapper_units).once('unselect-' + incompatible + '-units').click(function () {
              $(this, wrapper_units).closest('.form-checkboxes').find('.form-item-' + item.units_field_name + '-' + incompatible + ' input').prop('checked', false);
            });
          }
        });
        var wrapper_timeslots = '.' + item.wrapper_timeslots;
        if ($('.form-item-timeslots-all', wrapper_timeslots).length) {
          $('.form-item-timeslots-all input.form-checkbox', wrapper_timeslots).once('select-all-timeslots').click(function () {
            $(this, wrapper_timeslots).closest('.form-checkboxes').find('input.form-checkbox').prop('checked', $(this).prop('checked'));
          });
          $('.form-type-checkbox:not(.form-item-timeslots-all) input', wrapper_timeslots).once('unselect-all-timeslots').click(function () {
            $(this, wrapper_timeslots).closest('.form-checkboxes').find('.form-item-timeslots-all input').prop('checked', false);
          });
        }
      });
    }
  }

})(jQuery, Drupal, drupalSettings);
