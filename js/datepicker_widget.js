/**
 * @file
 * Your custom code into javascript behaviour.
 */

(function ($, Drupal, settings) {

  Drupal.behaviors.datepickerWidgetInit = {
    attach: function (context, settings) {
      var fields = drupalSettings.datepicker_widget.fields || [];
      fields.forEach(function (item) {
        $('.' + item.datepicker_disabled_dates).once().datepicker({
          changeMonth: item.datepicker_change_month_year,
          changeYear: item.datepicker_change_month_year,
          dateFormat: 'yy-mm-dd',
          numberOfMonths: item.datepicker_number_visible_months,
          selectMultiple: true,
          onSelect: function (dateText, inst) {
            var dates = $('.' + item.wrapper_disabled_dates).val().split(';');
            var gotDate = $.inArray(dateText, dates);
            if (gotDate >= 0) {
              dates.splice(gotDate,1);
            }
            else {
              dates.push(dateText);
            }
            $('.' + item.wrapper_disabled_dates).val(dates.join(';'));
          },
          beforeShowDay: function (date) {
            var dates = $('.' + item.wrapper_disabled_dates).val().split(';');
            var gotDate = $.inArray($.datepicker.formatDate($(this).datepicker('option', 'dateFormat'), date), dates);
            if (gotDate >= 0) {
              return [true, "ui-state-highlight", Drupal.t("Selected")];
            }
            return [true, ""];
          }
        })

      });
    }
  }

})(jQuery, Drupal, drupalSettings);
