<?php

namespace Drupal\entity_reservation_system\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ReservationSettings.
 */
class ReservationSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reservation_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'entity_reservation_system.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('entity_reservation_system.settings');

    $wrapper_base = 'entity-reservation-system-settings';
    $js_settings = [
      'wrapper_base' => $wrapper_base,
    ];

    $wraperDisabledDates = $wrapper_base . '-wrapper-disabled-dates';
    $js_settings['wrapper_disabled_dates'] = $wraperDisabledDates;
    $form['global_disabled_dates'] = [
      '#title' => $this->t('Disabled dates'),
      '#description' => $this->t('The selected dates are added to the global disabled dates if the "Use global disabled dates" are checked, if not checked only this dates are used.'),
      '#type' => 'textarea',
      '#required' => FALSE,
      '#default_value' => !empty($config->get('global_disabled_dates')) ? $config->get('global_disabled_dates') : NULL,
      '#attributes' => [
        'class' => [$wraperDisabledDates, 'reservation-hidden'],
      ],
    ];
    $datepickerDisabledDates = $wrapper_base . '-datepicker-disabled-dates';
    $js_settings['datepicker_disabled_dates'] = $datepickerDisabledDates;
    $js_settings['datepicker_number_visible_months'] = 12;
    $js_settings['datepicker_change_month_year'] = TRUE;
    $form['settings']['configuration']['datepicker'] = [
      '#markup' => '<div id="' . $datepickerDisabledDates . '" class="' . $datepickerDisabledDates . ' ui-reservation-slot-widget"></div>',
    ];

    $form['#attached']['library'][] = 'entity_reservation_system/datepicker_widget';
    $form['#attached']['drupalSettings']['datepicker_widget']['fields'][] = $js_settings;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('entity_reservation_system.settings');
    $config->set('global_disabled_dates', $form_state->getValue('global_disabled_dates'));
    $config->save();
  }

}
