<?php

namespace Drupal\entity_reservation_system\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_reservation_system\Entity\ReservationSlot as ReservationSlotEntity;
use Drupal\entity_reservation_system\TimeSlotsService;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\entity_reservation_system\ReservationsInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\message_notify\MessageNotifier;

/**
 * Class CreateReservationForm.
 */
class CreateReservationForm extends FormBase {

  /**
   * ID of the item to cancel.
   *
   * @var int
   */
  protected $reservation;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The timeSlot service.
   *
   * @var \Drupal\entity_reservation_system\TimeSlotsService
   */
  protected $timeSlotsService;

  /**
   * Variable that stores the user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The message notification service.
   *
   * @var \Drupal\message_notify\MessageNotifier
   */
  protected $messageNotifier;

  /**
   * The steps of the process.
   *
   * @var array
   */
  protected $steps = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_reservation_system.timeslots_service'),
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('message_notify.sender')
    );
  }

  /**
   * Overrides \Drupal\Core\Controller\ControllerBase::__construct().
   *
   * Overrides the construction of context aware to allow for
   * unvalidated constructor based injection of contexts.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The PrivateTempStore factory.
   * @param \Drupal\entity_reservation_system\TimeSlotsService $timeslots_service
   *   The timeslot service.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The account variable.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\message_notify\MessageNotifier $message_notifier
   *   The message notifier service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    TimeSlotsService $timeslots_service,
    AccountProxyInterface $account,
    ConfigFactoryInterface $config_factory,
    MessageNotifier $message_notifier
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->timeSlotsService = $timeslots_service;
    $this->account = $account;
    $this->configFactory = $config_factory;
    $this->messageNotifier = $message_notifier;
    $this->steps = $this->getDefaultSteps();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'create_reservation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $reservation = NULL, $config = NULL, $context = NULL) {
    $step = $form_state->get('step');
    if (empty($step)) {
      $step = 1;
      $form_state->set('step', $step);
    }
    switch ($step) {
      case 1:
        $form_state->set('config', $config);
        $form_state->set('context', $context);
        $form_state->set('reservation', $reservation);

        $wrapper_base = implode('-', array_map(function ($item) {
          return str_replace('_', '-', $item);
        }, $context));
        $js_settings = [
          'wrapper_base' => $wrapper_base,
        ];

        $js_settings['allow_all_calendar'] = $this->account->hasPermission("manage reservations for $context[entity_type] $context[bundle]") ? 1 : 0;
        $js_settings['first_day'] = $this->configFactory->get('system.date')->get('first_day');
        $js_settings['incompatible_units'] = [];

        // @TODO Change this for a service.
        $query = $this->entityTypeManager->getStorage('reservation_unit')->getQuery();
        $query->condition('entity_type', $reservation['entity_type']);
        $query->condition('entity_id', $reservation['entity_id']);
        $query->condition('status', 1);
        $result = $query->execute();
        $selectMethod = !empty($config['config']['select_multiple_units']) ? 'checkboxes' : 'radios';
        $optUnits = [];
        if (count($result)) {
          if (!empty($config['config']['select_all_units']) && count($result) > 1) {
            $optUnits['all'] = !empty($config['config']['select_all_units_label']) ? $config['config']['select_all_units_label'] : $this->t('Select all');
          }
          if ($result) {
            $units = $this->entityTypeManager->getStorage('reservation_unit')->loadMultiple($result);
            foreach ($units as $unit) {
              if ($unit->isIncompatible()) {
                $js_settings['incompatible_units'][] = $unit->id();
                $optUnits[$unit->id()] = $unit->label();
              }
              else {
                $js_settings['compatible_units'][] = $unit->id();
                $optUnits[$unit->id()] = $unit->label();
              }
            }
            $form_state->set('uncompatible_units', $js_settings['incompatible_units']);
          }
          // If there are incompatible units the all select has not effect.
          if (!empty($js_settings['incompatible_units'])) {
            unset($optUnits['all']);
          }
        }
        $js_settings['units_field_name'] = 'units';
        $wrapper_units = $wrapper_base . '-reservation-units';
        $js_settings['wrapper_units'] = $wrapper_units;
        $form['units'] = [
          '#type' => $selectMethod,
          '#title' => $selectMethod == 'checkboxes' ? $this->t('Units') : $this->t('Unit'),
          '#options' => $optUnits,
          '#required' => TRUE,
          '#attributes' => [
            'class' => [$wrapper_units],
          ],
        ];

        $wrapper_datevalue = $wrapper_base . '-reservation-datevalue';
        $wrapper_timeslots = $wrapper_base . '-reservation-timeslots';
        $js_settings['wrapper_datevalue'] = $wrapper_datevalue;
        $js_settings['wrapper_timeslots'] = $wrapper_timeslots;
        $form['datevalue'] = [
          '#title' => $this->t('Date'),
          '#type' => 'textfield',
          '#description' => $this->t('Select a date first. Then, choose from the available time slots.'),
          '#required' => TRUE,
          '#attributes' => [
            'class' => [$wrapper_datevalue, 'reservation-hidden'],
          ],
          '#ajax' => [
            'callback' => '::reloadTimeSlots',
            'event' => 'change',
            'wrapper' => $wrapper_timeslots,
          ],
        ];
        $wrapper_datepicker = $wrapper_base . '-reservation-datepicker';
        $js_settings['wrapper_datepicker'] = $wrapper_datepicker;
        $form['datepicker'] = [
          '#markup' => '<div class="' . $wrapper_datepicker . ' reservation-datepicker"></div>',
        ];

        $datevalue = $form_state->getValue('datevalue');
        $units_checked = $form_state->getValue('units');

        // Check if is a the refresh page request.
        if (empty($datevalue) && empty($units_checked) && !empty($form_state->getUserInput())) {
          $inputValues = $form_state->getUserInput();
          $datevalue = $inputValues['datevalue'];
          $units_checked = $inputValues['units'];
        }
        if (!is_array($units_checked)) {
          $units_checked = [$units_checked => $units_checked];
        }
        $selectMethod = !empty($config['config']['select_multiple_time_slots']) ? 'checkboxes' : 'radios';
        $optTimeSlots = [];
        if (!empty($datevalue) && !empty(array_filter($units_checked))) {
          unset($units_checked['all']);
          $units = array_intersect_key($units, array_filter($units_checked));
          $timeslots = $this->timeSlotsService->getTimeSlots($datevalue, $units, $config, $context);
          if (!empty($config['config']['select_all_time_slots']) && count($timeslots) > 1) {
            $optTimeSlots = ['all' => !empty($config['config']['select_all_time_slots_label']) ? $config['config']['select_all_time_slots_label'] : $this->t('Select all day')];
          }
          $optTimeSlots = $optTimeSlots + $timeslots;
        }
        $form['timeslots'] = [
          '#title' => $selectMethod == 'checkboxes' ? $this->t('Time slots') : $this->t('Time slot'),
          '#type' => $selectMethod,
          '#prefix' => '<div id="' . $wrapper_timeslots . '">',
          '#suffix' => '</div>',
          '#options' => $optTimeSlots,
          '#required' => TRUE,
          '#description' => empty($optTimeSlots) ? $this->t('There no are time slots available for the selected day.') : NULL,
          '#attributes' => [
            'class' => [$wrapper_timeslots],
          ],
        ];

        $form['#attached']['drupalSettings']['entity_reservation_system']['forms'][] = $js_settings;

        break;

      case 2:
        $reservation_customer = [
          'type' => 'reservation_customer',
          'uid' => $this->account->id(),
        ];
        $profile = $this->entityTypeManager->getStorage('profile')->create($reservation_customer);
        $form_state->set('reservation_customer', $reservation_customer);
        $form_display = EntityFormDisplay::collectRenderDisplay($profile, 'default');
        $form_display->buildForm($profile, $form, $form_state);
        break;

    }
    $form['actions'] = [
      '#type' => 'actions',
    ];
    if ($step > 1) {
      $form['actions']['return'] = [
        '#type' => 'submit',
        '#value' => $this->t('Previous'),
        '#limit_validation_errors' => [],
        '#weight' => 1,
        '#submit' => ['::returnPage'],
      ];
    }
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $step < (count($this->steps)) ? $this->t('Next') : $this->t('Reserve'),
      '#weight' => 2,
    ];

    $form['#attached']['library'][] = 'entity_reservation_system/reservations';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function reloadTimeSlots(array &$form, FormStateInterface $form_state) {
    return $form['timeslots'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $step = $form_state->get('step');
    if (empty($step)) {
      $step = 1;
    }
    if ($step == count($this->steps)) {
      $reservation_customer = $form_state->get('reservation_customer');
      $profile = $this->entityTypeManager->getStorage('profile')->create($reservation_customer);
      /** @var \Drupal\user\UserInterface $account */
      $form_display = EntityFormDisplay::collectRenderDisplay($profile, 'default');
      $form_display->extractFormValues($profile, $form, $form_state);
      $form_display->validateFormValues($profile, $form, $form_state);
      $profile->validate();
    }

    if ($step == 1) {
      $step1 = $form_state->getValues();
      if (!isset($step1['datevalue'])) {
        $step1 = $form_state->get('step_1');
      }
      $config = $form_state->get('config');
      $context = $form_state->get('context');

      // Check for uncompatible units selection.
      $uncompatible_units = $form_state->get('uncompatible_units');
      if (!empty($uncompatible_units)) {
        $selected_units = $step1['units'];
        if (!empty($uncompatible_units) && count(array_filter($selected_units)) > 1) {
          $form_state->setErrorByName('units', $this->t("The units selected are incompatible, please change your selection."));
        }
      }
      if (!is_array($step1['units'])) {
        $step1['units'] = [$step1['units'] => $step1['units']];
      }
      // Validate the timeslot always.
      $timeslots = $this->timeSlotsService->getTimeSlots($step1['datevalue'], $step1['units'], $config, $context);
      if(is_array($step1['timeslots'])){
        unset($step1['timeslots']['all']);
      }
      $prev = FALSE;
      foreach (array_filter($step1['timeslots']) as $timeslot) {
        if (!empty($config['config']['time_slots_consecutive'])) {
          if ($prev) {
            if (($timeslot - $prev) > $config['config']['slot_size']) {
              $form_state->setErrorByName('timeslots', $this->t('The selected timeslots must be consecutive.'));
            }
          }
          $prev = $timeslot;
        }
        if (empty($timeslots[$timeslot])) {
          $form_state->setErrorByName('timeslots', $this->t('One or more selected timeslots are unavailable.'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function returnPage(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
    $step = $form_state->get('step');
    $form_state->set('step_' . $step, $form_state->getValues());
    if (empty($step)) {
      $step = 1;
    }
    if ($step > 1) {
      $step--;
      $form_state->set('step', $step);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $step = $form_state->get('step');
    $form_state->set('step_' . $step, $form_state->getValues());
    if (empty($step)) {
      $step = 1;
    }
    switch ($step) {
      case 1:
        $step++;
        $form_state->set('step', $step);
        $form_state->setRebuild();
        break;

      case 2:
        $values = [];
        foreach ($this->steps as $stepkey => $steplabel) {
          $values[$stepkey] = $form_state->get('step_' . $stepkey);
        }

        $entityArray = $form_state->get('reservation');
        $config = $form_state->get('config');

        // Saving the customer profile before the reservation is created.
        $reservation_customer = $form_state->get('reservation_customer');
        $profile = $this->entityTypeManager->getStorage('profile')->create($reservation_customer);
        /** @var \Drupal\user\UserInterface $account */
        $form_display = EntityFormDisplay::collectRenderDisplay($profile, 'default');
        $form_display->extractFormValues($profile, $form, $form_state);
        $profile->save();
        $entityArray['contact_data']['target_id'] = $profile->id();
        $entityArray['reservation_units'] = !is_array($values[1]['units']) ? [$values[1]['units']] : array_filter($values[1]['units']);
        unset($entityArray['reservation_units']['all']);
        if(is_array($values[1]['timeslots'])) {
          unset($values[1]['timeslots']['all']);
        }
        $values[1]['timeslots'] = !is_array($values[1]['timeslots']) ? [$values[1]['timeslots']] : $values[1]['timeslots'];
        $entityArray['reserved_dates'] = [];
        if (is_array($values[1]['timeslots'])) {
          foreach ($values[1]['timeslots'] as $timeslot) {
            if (is_string($timeslot)) {
              $timeslot *= 60;
              $date = $values[1]['datevalue'] . ' ' . gmdate('H:i:s', $timeslot);
              $date = new DrupalDateTime($date, drupal_get_user_timezone());
              $format = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
              $entityArray['reserved_dates'][] = $date->format($format);
            }
          }
        }
        else {
          $timeslot = $values[1]['timeslots'] * 60;
          $date = $values[1]['datevalue'] . ' ' . gmdate('H:i:s', $timeslot);
          $date = new DrupalDateTime($date, drupal_get_user_timezone());
          $format = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
          $entityArray['reserved_dates'][] = $date->format($format);
        }
        $entityArray['expire'] = time() + ($config['config']['lock_minutes'] * 60);
        $entity = ReservationSlotEntity::create($entityArray);

        if (empty($config['config']['validation_is_required'])) {
          $entity->setReservationStatus(ReservationsInterface::RESERVATION_STATUS_CONFIRMED);
          $entity->status = 1;
        }
        elseif (empty($config['config']['payment_is_required'])) {
          $entity->setReservationStatus(ReservationsInterface::RESERVATION_STATUS_PENDING);
          $entity->status = 1;
        }
        $entity->save();
        $form_state->set('reservation_entity', $entity);
        $this->messenger()->addMessage($this->t('Your reservation has been created. An email with the details of it has been sent to your email account.'));

        break;

    }
  }

  /**
   * Get the default steps of the process.
   */
  public function getDefaultSteps() {
    return [
      1 => $this->t('Reservation preferences'),
      2 => $this->t('Contact data'),
    ];
  }

}
