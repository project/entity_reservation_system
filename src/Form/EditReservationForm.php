<?php

namespace Drupal\entity_reservation_system\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Drupal\message_notify\MessageNotifier;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class EditReservationForm.
 */
class EditReservationForm extends FormBase {
  /**
   * ID of the item to cancel.
   *
   * @var int
   */
  protected $reservation;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The message notification service.
   *
   * @var \Drupal\message_notify\MessageNotifier
   */
  protected $messageNotifier;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('message_notify.sender')
    );
  }

  /**
   * Overrides \Drupal\Core\Controller\ControllerBase::__construct().
   *
   * Overrides the construction of context aware to allow for
   * unvalidated constructor based injection of contexts.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The PrivateTempStore factory.
   * @param \Drupal\message_notify\MessageNotifier $message_notifier
   *   The message notifier service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    MessageNotifier $message_notifier,
    ConfigFactoryInterface $config_factory
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->messageNotifier = $message_notifier;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'edit_reservation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $reservation = NULL) {
    if (!$reservation) {
      throw new BadRequestHttpException($this->t('Reservable not found.'));
    }

    $form_state->set('reservation', $reservation);

    $view_builder = $this->entityTypeManager->getViewBuilder('reservation_slot');
    $form['reservation'] = $view_builder->view($reservation);

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Please, describe the reason of the change that you want to do on the reservation.'),
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $system_site_config = $this->configFactory->get('system.site');
    $site_email = $system_site_config->get('mail');
    $entity = $form_state->get('reservation');
    $message = $this->entityTypeManager->getStorage('message')->create([
      'template' => 'reservation_contact',
      'field_reservation' => $entity->id(),
      'field_message_content' => $form_state->getValue('description'),
      'uid' => $entity->getOwnerId(),
    ]);
    $message->save();
    if ($message) {
      $this->messageNotifier->send($message, []);
      $this->messageNotifier->send($message, ['mail' => $site_email]);
    }
  }

}
