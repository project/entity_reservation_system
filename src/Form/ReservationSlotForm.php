<?php

namespace Drupal\entity_reservation_system\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\entity_reservation_system\ReservationsInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\entity_reservation_system\TimeSlotsService;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\message_notify\MessageNotifier;

/**
 * Form controller for Reservation slot edit forms.
 *
 * @ingroup entity_reservation_system
 */
class ReservationSlotForm extends ContentEntityForm {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The timeslot service.
   *
   * @var \Drupal\entity_reservation_system\TimeSlotsService
   */
  protected $timeSlotsService;

  /**
   * The message notification service.
   *
   * @var \Drupal\message_notify\MessageNotifier
   */
  protected $messageNotifier;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The PrivateTempStore factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\entity_reservation_system\TimeSlotsService $timeslots_service
   *   The timeslot service.
   * @param \Drupal\message_notify\MessageNotifier $message_notifier
   *   The message notifier service.
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
    TimeInterface $time = NULL,
    AccountInterface $current_user,
    EntityTypeManagerInterface $entityTypeManager,
    ConfigFactoryInterface $config_factory,
    TimeSlotsService $timeslots_service,
    MessageNotifier $message_notifier
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $config_factory;
    $this->timeSlotsService = $timeslots_service;
    $this->messageNotifier = $message_notifier;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('entity_reservation_system.timeslots_service'),
      $container->get('message_notify.sender')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\entity_reservation_system\Entity\ReservationSlot */
    $form = parent::buildForm($form, $form_state);
    $timeZone = drupal_get_user_timezone();
    $hostEntity = $this->entityTypeManager->getStorage($this->entity->getHostEntityType())->load($this->entity->getHostEntityId());
    $field_name = $this->entity->getHostFieldName();
    $delta = $this->entity->getHostFieldDelta();
    $config = $hostEntity->{$field_name}[$delta]->getPropertyValues();

    $context = [
      'entity_type' => $this->entity->getHostEntityType(),
      'bundle' => $hostEntity->bundle(),
      'entity_id' => $this->entity->getHostEntityId(),
      'field_name' => $field_name,
      'delta' => $delta,
    ];

    $form_state->set('host_entity', $hostEntity);
    $form_state->set('config', $config);
    $form_state->set('context', $context);

    $wrapper_base = implode('-', array_map(function ($item) {
      return str_replace('_', '-', $item);
    }, $context));
    $js_settings = [
      'wrapper_base' => $wrapper_base,
    ];

    $js_settings['allow_all_calendar'] = $this->currentUser->hasPermission("manage reservations for $context[entity_type] $context[bundle]") ? 1 : 0;
    $js_settings['first_day'] = $this->configFactory->get('system.date')->get('first_day');

    $units = [];
    $unitsStorage = $this->entityTypeManager->getStorage('reservation_unit');
    $query = $unitsStorage->getQuery();
    $query->condition('entity_type', $this->entity->getHostEntityType());
    $query->condition('entity_id', $this->entity->getHostEntityId());
    $query->condition('status', 1);
    $result = $query->execute();
    $selectMethod = !empty($config['config']['select_multiple_units']) ? 'checkboxes' : 'radios';
    $optUnits = [];
    if (count($result)) {
      if (!empty($config['config']['select_all_units']) && count($result) > 1) {
        $optUnits['all'] = !empty($config['config']['select_all_units_label']) ? $config['config']['select_all_units_label'] : $this->t('Select all');
      }
      if ($result) {
        $units = $unitsStorage->loadMultiple($result);
        foreach ($units as $unit) {
          if ($unit->isIncompatible()) {
            $js_settings['incompatible_units'][] = $unit->id();
            $optUnits[$unit->id()] = $unit->label();
          }
          else {
            $js_settings['compatible_units'][] = $unit->id();
            $optUnits[$unit->id()] = $unit->label();
          }
        }
        $form_state->set('uncompatible_units', $js_settings['incompatible_units']);
      }
      // If there are incompatible units the all select has not effect.
      if (!empty($js_settings['incompatible_units'])) {
        unset($optUnits['all']);
      }
    }
    $js_settings['units_field_name'] = 'reservation-units';
    $wrapper_units = $wrapper_base . '-reservation-units';
    $js_settings['wrapper_units'] = $wrapper_units;
    $form['reservation_units']['#weight'] = 0;
    $form['reservation_units']['widget']['#options'] = $optUnits;
    $form['reservation_units']['widget']['#attributes']['class'][] = $wrapper_units;
    $form['reservation_units']['widget']['#type'] = $selectMethod;
    $form['reservation_units']['widget']['#title'] = $selectMethod == 'checkboxes' ? $this->t('Units') : $this->t('Unit');
    $originalUnits = $form['reservation_units']['widget']['#default_value'];

    $form['reserved_dates']['#access'] = FALSE;

    $originalDates = '';
    $originalTimeSlots = [];
    foreach ($this->entity->reserved_dates as $date) {
      if (!empty($date->value)) {
        $date = new DrupalDateTime($date->value, $timeZone);
        $originalDates = $date->format('Y-m-d', 'custom', $timeZone);
        $originalTimeSlots[] = ($date->format('H', 'custom', $timeZone) * 60) + $date->format('i', 'custom', 'UTC');
      }
    }
    $originalDates = !empty($form_state->get('originalDate')) ? $form_state->get('originalDate') : $originalDates;
    $form_state->set('originalDate', NULL);

    $wrapper_datevalue = $wrapper_base . '-reservation-datevalue';
    $wrapper_timeslots = $wrapper_base . '-reservation-timeslots';
    $js_settings['wrapper_datevalue'] = $wrapper_datevalue;
    $js_settings['wrapper_timeslots'] = $wrapper_timeslots;
    $form['datevalue'] = [
      '#weight' => 1,
      '#title' => $this->t('Date'),
      '#type' => 'textfield',
      '#default_value' => $originalDates,
      '#required' => TRUE,
      '#attributes' => [
        'class' => [$wrapper_datevalue, 'reservation-hidden'],
      ],
      '#ajax' => [
        'callback' => '::reloadTimeSlots',
        'event' => 'change',
        'wrapper' => $wrapper_timeslots,
      ],
    ];
    $wrapper_datepicker = $wrapper_base . '-reservation-datepicker';
    $js_settings['wrapper_datepicker'] = $wrapper_datepicker;
    $form['datepicker'] = [
      '#markup' => '<div class="' . $wrapper_datepicker . ' reservation-datepicker"></div>',
      '#weight' => 1,
    ];

    $values = $form_state->getValues();
    $datevalue = $originalDates;
    if (!empty($values['reserved_dates'])) {
      $originalTimeSlots = [];
      foreach ($values['reserved_dates'] as $reserved_date) {
        $datevalue = $reserved_date['value']->format('Y-m-d', 'custom', $timeZone);
        $timeslot = ($reserved_date['value']->format('H', 'custom', $timeZone) * 60) + $reserved_date['value']->format('i', 'custom', 'UTC');
        $originalTimeSlots[$timeslot] = $timeslot;
      }
    }
    $units_values = !empty($form_state->getValue('reservation_units')) ? $form_state->getValue('reservation_units') : $originalUnits;
    $units_checked = [];
    if (!empty($units_values)) {
      foreach ($units_values as $units_value) {
        if (isset($units_value['target_id'])) {
          $units_checked[$units_value['target_id']] = $units_value['target_id'];
        }
        else {
          $units_checked[$units_value] = $units_value;
        }
      }
    }
    // Check if is a the refresh page request.
    if (empty($datevalue) && empty($units_checked) && !empty($form_state->getUserInput())) {
      $inputValues = $form_state->getUserInput();
      $datevalue = $inputValues['datevalue'];
      $units_checked = $inputValues['units'];
    }
    if (!is_array($units_checked)) {
      $units_checked = [$units_checked => $units_checked];
    }
    $selectMethod = !empty($config['config']['select_multiple_time_slots']) ? 'checkboxes' : 'radios';
    $optTimeSlots = [];
    if (!empty($datevalue) && !empty(array_filter($units_checked))) {
      unset($units_checked['all']);
      $units = array_intersect_key($units, array_filter($units_checked));
      $timeslots = $this->timeSlotsService->getTimeSlots($datevalue, $units, $config, $context, $originalTimeSlots);
      if (!empty($config['config']['select_all_time_slots']) && count($timeslots) > 1) {
        $optTimeSlots = ['all' => !empty($config['config']['select_all_time_slots_label']) ? $config['config']['select_all_time_slots_label'] : $this->t('Select all day')];
      }
      $optTimeSlots = $optTimeSlots + $timeslots;
    }
    $form['timeslots'] = [
      '#weight' => 1,
      '#title' => $selectMethod == 'checkboxes' ? $this->t('Time slots') : $this->t('Time slot'),
      '#type' => $selectMethod,
      '#prefix' => '<div id="' . $wrapper_timeslots . '">',
      '#suffix' => '</div>',
      '#options' => $optTimeSlots,
      '#default_value' => $originalTimeSlots,
      '#required' => TRUE,
      '#description' => empty($optTimeSlots) ? $this->t('There no are time slots available for the selected day.') : NULL,
      '#attributes' => [
        'class' => [$wrapper_timeslots],
      ],
    ];

    $form['contact_data']['#weight'] = 4;

    $allowed_options = [
      ReservationsInterface::RESERVATION_STATUS_PENDING => ReservationsInterface::RESERVATION_STATUS_PENDING_LABEL,
      ReservationsInterface::RESERVATION_STATUS_CONFIRMED => ReservationsInterface::RESERVATION_STATUS_CONFIRMED_LABEL,
      ReservationsInterface::RESERVATION_STATUS_CANCELLED => ReservationsInterface::RESERVATION_STATUS_CANCELLED_LABEL,
    ];
    $form['reservation_status']['widget']['#options'] = $allowed_options;
    $form['reservation_status']['#weight'] = 6;

    $form['uid']['#access'] = FALSE;
    $form['status']['#access'] = FALSE;

    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
    }

    $form['#attached']['library'][] = 'entity_reservation_system/reservations';
    $form['#attached']['drupalSettings']['entity_reservation_system']['forms'][] = $js_settings;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function reloadTimeSlots(array &$form, FormStateInterface $form_state) {
    return $form['timeslots'];
  }

  /**
   * {@inheritdoc}
   *
   * Button-level validation handlers are highly discouraged for entity forms,
   * as they will prevent entity validation from running. If the entity is going
   * to be saved during the form submission, this method should be manually
   * invoked from the button-level validation handler, otherwise an exception
   * will be thrown.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getValue('datevalue')) && !empty($form_state->getValue('timeslots'))) {
      $originalDate = $form_state->getValue('datevalue');
      $form_state->setValue('datevalue', NULL);
      $timeslots = $form_state->getValue('timeslots');
      $form_state->setValue('timeslots', NULL);
      $timeslots = array_filter($timeslots);
      $dateValues = [];
      $storage_timezone = new \DateTimezone('UTC');
      foreach ($timeslots as $timeslot) {
        $timeslot *= 60;
        $date = $originalDate . ' ' . gmdate('H:i:s', $timeslot);
        $date = new DrupalDateTime($date, 'UTC');
        $dateValues[]['value'] = $date->setTimezone($storage_timezone);
      }
      if (empty($dateValues)) {
        $form_state->set('originalDate', $originalDate);
      }
      $form_state->setValue('reserved_dates', $dateValues);
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime(REQUEST_TIME);
      $entity->setRevisionUserId($this->currentUser->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Reservation slot.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Reservation slot.', [
          '%label' => $entity->label(),
        ]));

    }
    $form_state->setRedirect('entity.reservation_slot.canonical', ['reservation_slot' => $entity->id()]);
  }

}
