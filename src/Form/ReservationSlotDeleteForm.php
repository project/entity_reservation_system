<?php

namespace Drupal\entity_reservation_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Reservation slot entities.
 *
 * @ingroup entity_reservation_system
 */
class ReservationSlotDeleteForm extends ContentEntityDeleteForm {
}
