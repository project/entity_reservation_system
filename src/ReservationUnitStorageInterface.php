<?php

namespace Drupal\entity_reservation_system;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\entity_reservation_system\Entity\ReservationUnitInterface;

/**
 * Defines the storage handler class for Reservation unit entities.
 *
 * This extends the base storage class, adding required special handling for
 * Reservation unit entities.
 *
 * @ingroup entity_reservation_system
 */
interface ReservationUnitStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Reservation unit revision IDs for a specific Unit.
   *
   * @param \Drupal\entity_reservation_system\Entity\ReservationUnitInterface $entity
   *   The Reservation unit entity.
   *
   * @return int[]
   *   Reservation unit revision IDs (in ascending order).
   */
  public function revisionIds(ReservationUnitInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Reservation unit author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Reservation unit revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\entity_reservation_system\Entity\ReservationUnitInterface $entity
   *   The Reservation unit entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(ReservationUnitInterface $entity);

  /**
   * Unsets the language for all Reservation unit with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
