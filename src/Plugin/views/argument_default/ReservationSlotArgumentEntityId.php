<?php

namespace Drupal\entity_reservation_system\Plugin\views\argument_default;

/**
 * Default argument plugin to extract a node.
 *
 * @ViewsArgumentDefault(
 *   id = "reservtion_slot_argument_entity_id",
 *   title = @Translation("The current entity id of a reservation slot")
 * )
 */
class ReservationSlotArgumentEntityId extends ReservationSlotArgumentBase {

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    $reservation_slot = $this->currentRouteMatch->getParameter('reservation_slot');
    if ($reservation_slot && !empty($reservation_slot->getHostEntityId())) {
      return $reservation_slot->getHostEntityId();
    }
    return '';
  }

}
