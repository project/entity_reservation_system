<?php

namespace Drupal\entity_reservation_system\Plugin\views\argument_default;

/**
 * Default argument plugin to extract a node.
 *
 * @ViewsArgumentDefault(
 *   id = "reservtion_slot_argument_entity_type",
 *   title = @Translation("The current entity type of a reservation slot")
 * )
 */
class ReservationSlotArgumentEntityType extends ReservationSlotArgumentBase {

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    $reservation_slot = $this->currentRouteMatch->getParameter('reservation_slot');
    if ($reservation_slot && !empty($reservation_slot->getHostEntityType())) {
      return $reservation_slot->getHostEntityType();
    }
    return '';
  }

}
