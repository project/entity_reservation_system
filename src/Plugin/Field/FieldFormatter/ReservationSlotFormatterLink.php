<?php

namespace Drupal\entity_reservation_system\Plugin\Field\FieldFormatter;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\entity_reservation_system\ReservationsInterface;

/**
 * Provides a default reservation formatter.
 *
 * @FieldFormatter(
 *   id = "reservation_slot_link",
 *   module = "reservation",
 *   label = @Translation("Reservation link"),
 *   field_types = {
 *     "reservation_slot"
 *   },
 *   quickedit = {
 *     "editor" = "disabled"
 *   }
 * )
 */
class ReservationSlotFormatterLink extends ReservationSlotFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // The entity that contains the reservation.
    $source_entity = $items->getEntity();

    // Determine if reservation is previewed within a Paragraph on
    // node edit forms (via *.edit_form or .content_translation_add routes).
    $route = $this->routeMatch->getRouteName();
    $config = $items->first()->getPropertyValues();

    $is_node_edit = (preg_match('/\.edit_form$/', $route) || preg_match('/\.content_translation_add$/', $route));
    $is_paragraph = ($source_entity && $source_entity->getEntityTypeId() === 'paragraph');
    $is_paragraph_node_edit = ($is_paragraph && $is_node_edit);

    $elements = [];
    foreach ($items as $delta => $configuration) {
      if (!$is_paragraph_node_edit) {
        $arguments = [
          'entity_type' => $source_entity->getEntityTypeId(),
          'bundle' => $source_entity->bundle(),
          'entity_id' => $source_entity->id(),
          'field_name' => $this->fieldDefinition->getName(),
          'delta' => $delta,
        ];
        $route = Url::fromRoute('entity_reservation_system.create_reservation_form', $arguments, ['query' => $this->redirectDestination->getAsArray()]);
        $elements[$delta] = [
          '#type' => 'link',
          '#url' => $route,
          '#title' => $this->t('Create a reservation'),
        ];
        if (!empty($configuration->config['require_login']) && $this->account->isAnonymous()) {
          $route = Url::fromRoute('user.login', [], ['query' => $this->redirectDestination->getAsArray()]);
          $elements[$delta]['#url'] = $route;
        }
        else {
          switch ($config['status']) {
            case ReservationsInterface::STATUS_CLOSED:
              $elements[$delta] = [
                '#type' => 'html_tag',
                '#tag' => 'div',
                '#value' => $this->t('Reservations are not accepted at this time'),
              ];
              break;
            case ReservationsInterface::STATUS_OPEN:
              if (!$this->account->hasPermission('add reservation slot')) {
                $elements[$delta] = [
                  '#type' => 'html_tag',
                  '#tag' => 'div',
                  '#value' => $this->t('Your user cannot do reservations'),
                ];
              }
              break;
            case ReservationsInterface::STATUS_PRIVATE:
              if (!$this->account->hasPermission('add private reservation slot')) {
                $elements[$delta] = [
                  '#type' => 'html_tag',
                  '#tag' => 'div',
                  '#value' => $this->t('Your user cannot do reservations'),
                ];
              }
              break;
            case ReservationsInterface::STATUS_SCHEDULED:
              if ($this->account->hasPermission('add reservation slot')) {
                $currentDate = date(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
                if ($currentDate < $config['open'] || $currentDate > $config['close']) {
                  $elements[$delta] = [
                    '#type' => 'html_tag',
                    '#tag' => 'div',
                    '#value' => $this->t('The reservation period is currently closed'),
                  ];
                }
              }
              else {
                $elements[$delta] = [
                  '#type' => 'html_tag',
                  '#tag' => 'div',
                  '#value' => $this->t('Your user cannot do reservations'),
                ];
              }
              break;
            case ReservationsInterface::STATUS_PRIVATE_SCHEDULED:
              $currentDate = date(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
              if (!$this->account->hasPermission('add private reservation slot')) {
                $elements[$delta] = [
                  '#type' => 'html_tag',
                  '#tag' => 'div',
                  '#value' => $this->t('Your user cannot do reservations'),
                ];
              }
              elseif ($currentDate < $config['open'] ||  $currentDate > $config['close']) {
                $elements[$delta] = [
                  '#type' => 'html_tag',
                  '#tag' => 'div',
                  '#value' => $this->t('The reservation period is currently closed'),
                ];

              }
              break;
          }
        }

      }
      $this->setCacheContext($elements[$delta], $configuration);
    }

    return $elements;
  }

}
