<?php

namespace Drupal\entity_reservation_system\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;

/**
 * Provides a default reservation formatter.
 *
 * @FieldFormatter(
 *   id = "reservation_slot_default",
 *   module = "reservation",
 *   label = @Translation("Reservation form"),
 *   field_types = {
 *     "reservation_slot"
 *   },
 *   quickedit = {
 *     "editor" = "disabled"
 *   }
 * )
 */
class ReservationSlotFormatterDefault extends ReservationSlotFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // The entity that contains the reservation.
    $source_entity = $items->getEntity();

    // Determine if reservation is previewed within a Paragraph on
    // node edit forms (via *.edit_form or .content_translation_add routes).
    $route = $this->routeMatch->getRouteName();
    $is_node_edit = (preg_match('/\.edit_form$/', $route) || preg_match('/\.content_translation_add$/', $route));
    $is_paragraph = ($source_entity && $source_entity->getEntityTypeId() === 'paragraph');
    $is_paragraph_node_edit = ($is_paragraph && $is_node_edit);

    $elements = [];
    foreach ($items as $delta => $configuration) {
      if (!$is_paragraph_node_edit) {
        if (!empty($configuration->config['require_login']) && $this->account->isAnonymous()) {
          $user_login = Url::fromRoute('user.login', [], ['query' => $this->redirectDestination->getAsArray()]);
          $user_register = Url::fromRoute('user.register', [], ['query' => $this->redirectDestination->getAsArray()]);
          $elements[$delta] = [
            '#markup' => $this->t('Please, <a href="@login">login</a> or <a href="@register">register</a> to continue', ['@login' => $user_login->toString(), '@register' => $user_register->toString()]),
          ];
        }
        else {
          $elements[$delta] = [
            '#type' => 'reservation_slot',
            '#entity' => $source_entity,
            '#configuration' => $configuration->getPropertyValues(),
            '#field_name' => $this->fieldDefinition->getName(),
            '#delta' => $delta,
          ];
        }
      }
      else {

      }
      $this->setCacheContext($elements[$delta], $configuration);
    }
    return $elements;
  }

}
