<?php

namespace Drupal\entity_reservation_system\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_reservation_system\ReservationsInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Core\Datetime\DateHelper;
use Drupal\Component\Utility\NestedArray;

/**
 * Provides a default reservation widget.
 *
 * @FieldWidget(
 *   id = "reservation_slot_default",
 *   label = @Translation("Reservation"),
 *   field_types = {
 *     "reservation_slot"
 *   }
 * )
 */
class ReservationSlotWidgetDefault extends ReservationSlotWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $date_helper = new DateHelper();

    $wrapper_base = str_replace('_', '-', $this->fieldDefinition->getName() . '-' . $delta);
    $js_settings = [
      'wrapper_base' => $wrapper_base,
    ];

    // Set item default status to open.
    if (!isset($items[$delta]->status)) {
      $items[$delta]->status = ReservationsInterface::STATUS_OPEN;
    }

    // Get field name.
    $field_name = $items->getName();

    // Get field input name from field parents, field name, and the delta.
    $field_parents = array_merge($element['#field_parents'], [$field_name, $delta]);
    $field_input_name = (array_shift($field_parents)) . ('[' . implode('][', $field_parents) . ']');

    $element['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Reservation settings'),
      '#element_validate' => [[$this, 'validateOpenClose']],
      '#open' => TRUE,
    ];

    $element['settings']['availability'] = [
      '#type' => 'details',
      '#title' => $this->t('Publish options'),
      '#element_validate' => [[$this, 'validateOpenClose']],
      '#open' => TRUE,
    ];
    $element['settings']['availability']['status'] = [
      '#type' => 'radios',
      '#title' => $this->t('Status'),
      '#options' => [
        ReservationsInterface::STATUS_OPEN => $this->t(ReservationsInterface::STATUS_OPEN_LABEL),
        ReservationsInterface::STATUS_PRIVATE => $this->t(ReservationsInterface::STATUS_PRIVATE_LABEL),
        ReservationsInterface::STATUS_CLOSED => $this->t(ReservationsInterface::STATUS_CLOSED_LABEL),
        ReservationsInterface::STATUS_SCHEDULED => $this->t(ReservationsInterface::STATUS_SCHEDULED_LABEL),
        ReservationsInterface::STATUS_PRIVATE_SCHEDULED => $this->t(ReservationsInterface::STATUS_PRIVATE_SCHEDULED_LABEL),
      ],
      '#options_display' => 'side_by_side',
      '#default_value' => isset($items[$delta]->status) ? $items[$delta]->status : NULL,
    ];

    $element['settings']['availability']['scheduled'] = [
      '#type' => 'item',
      '#title' => $this->t('Reservation scheduling'),
      '#title_display' => 'invisible',
      '#input' => FALSE,
      '#states' => [
        'visible' => [
          ['input[name="' . $field_input_name . '[settings][availability][status]"]' => ['value' => ReservationsInterface::STATUS_SCHEDULED]],
          'or',
          ['input[name="' . $field_input_name . '[settings][availability][status]"]' => ['value' => ReservationsInterface::STATUS_PRIVATE_SCHEDULED]],
        ],
      ],
    ];
    $element['settings']['availability']['scheduled']['open'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Open'),
      '#default_value' => $items[$delta]->open ? DrupalDateTime::createFromTimestamp(strtotime($items[$delta]->open)) : NULL,
      '#prefix' => '<div class="container-inline form-item">',
      '#suffix' => '</div>',
    ];
    $element['settings']['availability']['scheduled']['close'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Close'),
      '#default_value' => $items[$delta]->close ? DrupalDateTime::createFromTimestamp(strtotime($items[$delta]->close)) : NULL,
      '#prefix' => '<div class="container-inline form-item">',
      '#suffix' => '</div>',
    ];

    $configuration = isset($items[$delta]->config) ? $items[$delta]->config : [];
    $element['settings']['configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Configuration'),
      '#element_validate' => [[$this, 'validateOpenClose']],
      '#open' => TRUE,
    ];

    $element['settings']['configuration']['require_login'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add login or register links for anonymous users'),
      '#default_value' => isset($configuration['require_login']) ? $configuration['require_login'] : NULL,
    ];

    $element['settings']['configuration']['slot_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum reservation time'),
      '#min' => 5,
      '#max' => 1440,
      '#step' => 5,
      '#field_suffix' => $this->t('minutes'),
      '#required' => TRUE,
      '#default_value' => isset($configuration['slot_size']) ? $configuration['slot_size'] : NULL,
      '#description' => $this->t('The reservation will a sum of blocks of this size in minutes.'),
    ];

    $element['settings']['configuration']['select_multiple_units'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow multiple units'),
      '#default_value' => isset($configuration['select_multiple_units']) ? $configuration['select_multiple_units'] : NULL,
    ];

    $element['settings']['configuration']['select_all_units'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show option to select all units'),
      '#default_value' => isset($configuration['select_all_units']) ? $configuration['select_all_units'] : NULL,
      '#states' => [
        'visible' => [
          ['input[name="' . $field_input_name . '[settings][configuration][select_multiple_units]"]' => ['checked' => TRUE]],
        ],
        'unchecked' => [
          ['input[name="' . $field_input_name . '[settings][configuration][select_multiple_units]"]' => ['checked' => FALSE]],
        ],
      ],
    ];

    $element['settings']['configuration']['select_all_units_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text to show in select all units option'),
      '#default_value' => isset($configuration['select_all_units_label']) ? $configuration['select_all_units_label'] : NULL,
      '#states' => [
        'visible' => [
          ['input[name="' . $field_input_name . '[settings][configuration][select_all_units]"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    $element['settings']['configuration']['select_multiple_time_slots'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow select multiple time slots'),
      '#default_value' => isset($configuration['select_multiple_time_slots']) ? $configuration['select_multiple_time_slots'] : NULL,
    ];

    $element['settings']['configuration']['select_all_time_slots'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show option to select all time slots'),
      '#default_value' => isset($configuration['select_all_time_slots']) ? $configuration['select_all_time_slots'] : NULL,
      '#states' => [
        'visible' => [
          ['input[name="' . $field_input_name . '[settings][configuration][select_multiple_time_slots]"]' => ['checked' => TRUE]],
        ],
        'unchecked' => [
          ['input[name="' . $field_input_name . '[settings][configuration][select_multiple_time_slots]"]' => ['checked' => FALSE]],
        ],
      ],
    ];

    $element['settings']['configuration']['select_all_time_slots_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text to show in select all time slots option'),
      '#default_value' => isset($configuration['select_all_time_slots_label']) ? $configuration['select_all_time_slots_label'] : NULL,
      '#states' => [
        'visible' => [
          ['input[name="' . $field_input_name . '[settings][configuration][select_all_time_slots]"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    $element['settings']['configuration']['time_slots_consecutive'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reserved slots must be consecutive'),
      '#default_value' => isset($configuration['time_slots_consecutive']) ? $configuration['time_slots_consecutive'] : NULL,
      '#states' => [
        'visible' => [
          ['input[name="' . $field_input_name . '[settings][configuration][select_multiple_time_slots]"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    $element['settings']['configuration']['lock_minutes'] = [
      '#type' => 'number',
      '#title' => $this->t('Lock minutes'),
      '#min' => 0,
      '#max' => 1440,
      '#field_suffix' => $this->t('minutes'),
      '#required' => TRUE,
      '#default_value' => isset($configuration['lock_minutes']) ? $configuration['lock_minutes'] : NULL,
      '#description' => $this->t('This is the time that the reservation is conserved until it expires if not completed.'),
    ];

    $element['settings']['configuration']['validation_is_required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Validation is required'),
      '#description' => $this->t('If this checkbox is checked the reservations are stored automatically as pending.'),
      '#default_value' => isset($configuration['validation_is_required']) ? $configuration['validation_is_required'] : NULL,
    ];

    if ($this->moduleHandler->moduleExists('entity_reservation_system_order')) {
      $element['settings']['configuration']['payment_is_required'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Locked until payment'),
        '#description' => $this->t('If this checkbox is checked the reservations wait locked until the user finishes the payment. The prices are stored per units.'),
        '#default_value' => isset($configuration['payment_is_required']) ? $configuration['payment_is_required'] : NULL,
      ];
    }

    $element['settings']['configuration']['global_disabled_dates'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use global disabled dates'),
      '#default_value' => isset($configuration['select_multiple_units']) ? $configuration['select_multiple_units'] : NULL,
    ];

    $element['settings']['configuration']['global_disabled_dates'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use global disabled dates'),
      '#default_value' => isset($configuration['select_multiple_units']) ? $configuration['select_multiple_units'] : NULL,
    ];

    $wraperDisabledDates = $wrapper_base . '-wrapper-disabled-dates';
    $js_settings['wrapper_disabled_dates'] = $wraperDisabledDates;
    $element['settings']['configuration']['disabled_dates'] = [
      '#title' => $this->t('Disabled dates'),
      '#description' => $this->t('The selected dates are added to the global disabled dates if the "Use global disabled dates" are checked, if not checked only this dates are used.'),
      '#type' => 'textarea',
      '#required' => FALSE,
      '#default_value' => !empty($configuration['disabled_dates']) ? $configuration['disabled_dates'] : NULL,
      '#attributes' => [
        'class' => [$wraperDisabledDates, 'reservation-hidden'],
      ],
    ];
    $datepickerDisabledDates = $wrapper_base . '-datepicker-disabled-dates';
    $js_settings['datepicker_disabled_dates'] = $datepickerDisabledDates;
    $js_settings['datepicker_number_visible_months'] = 2;
    $js_settings['datepicker_change_month_year'] = TRUE;
    $element['settings']['configuration']['datepicker'] = [
      '#markup' => '<div id="' . $datepickerDisabledDates . '" class="' . $datepickerDisabledDates . ' ui-reservation-slot-widget"></div>',
    ];

    $element['settings']['configuration']['week_schedule'] = [
      '#type' => 'details',
      '#title' => $this->t('Week Schedule'),
      '#element_validate' => [[$this, 'validateWeekSchedule']],
      '#open' => TRUE,
    ];

    // Prepare the days of the week.
    $week_days = $date_helper->weekDays(TRUE);
    $week_days = $date_helper->weekDaysOrdered($week_days);

    $days_abbr = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
    $days_abbr = $date_helper->weekDaysOrdered($days_abbr);

    $days = [];
    foreach ($days_abbr as $days_abbr_key => $days_abbr_value) {
      $days[strtolower($days_abbr_value)] = $week_days[$days_abbr_key];
    }

    $timing = isset($configuration['week_schedule']) ? $configuration['week_schedule'] : [];

    $minute_options = ['' => ''] + array_combine(range(0, 59), range(0, 59));
    foreach ($days as $day_key => $day_name) {
      $element['settings']['configuration']['week_schedule'][$day_key] = [
        '#type' => 'container',
      ];
      $element['settings']['configuration']['week_schedule'][$day_key]['enable'] = [
        '#type' => 'checkbox',
        '#title' => $day_name,
        '#default_value' => isset($timing[$day_key]['enable']) ? $timing[$day_key]['enable'] : NULL,
      ];
      $baseName = $this->fieldDefinition->getName() . '_' . $delta . '_' . $day_key;
      $scheduleId = str_replace('_', '-', $baseName) . '-schedule';
      $element['settings']['configuration']['week_schedule'][$day_key]['schedule'] = [
        '#type' => 'container',
        '#prefix' => '<div id="' . $scheduleId . '">',
        '#suffix' => '</div>',
        '#states' => [
          'visible' => [
            ':input[name="' . $this->getCheckBaseNameByElement($element) . '[' . $delta . '][settings][configuration][week_schedule][' . $day_key . '][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $deltas = $form_state->get($baseName . '_deltas');
      if (!isset($deltas) && count($timing[$day_key]['schedule'])) {
        $deltas = count($timing[$day_key]['schedule']);
        $form_state->set($baseName . '_deltas', $deltas);
      }
      for ($i = 0; $i < $deltas; $i++) {
        $element['settings']['configuration']['week_schedule'][$day_key]['schedule'][$i] = [
          '#type' => 'container',
          '#attributes' => ['class' => ['form--inline']],
        ];

        $element['settings']['configuration']['week_schedule'][$day_key]['schedule'][$i]['start_time_hour'] = [
          '#type' => 'select',
          '#title' => $this->t('Hour'),
          '#options' => $date_helper->hours('G', FALSE),
          '#default_value' => isset($timing[$day_key]['schedule'][$i]['start_time_hour']) ? $timing[$day_key]['schedule'][$i]['start_time_hour'] : NULL,
        ];
        $element['settings']['configuration']['week_schedule'][$day_key]['schedule'][$i]['start_time_minute'] = [
          '#type' => 'select',
          '#title' => $this->t('Minute'),
          '#options' => $minute_options,
          '#default_value' => isset($timing[$day_key]['schedule'][$i]['start_time_minute']) ? $timing[$day_key]['schedule'][$i]['start_time_minute'] : NULL,
        ];
        $element['settings']['configuration']['week_schedule'][$day_key]['schedule'][$i]['separator'] = [
          '#markup' => '<span class="form-item hour-separator"> ' . $this->t('until') . ' </span>',
        ];
        $element['settings']['configuration']['week_schedule'][$day_key]['schedule'][$i]['end_time_hour'] = [
          '#type' => 'select',
          '#title' => $this->t('Hour'),
          '#options' => $date_helper->hours('G', FALSE),
          '#default_value' => isset($timing[$day_key]['schedule'][$i]['end_time_hour']) ? $timing[$day_key]['schedule'][$i]['end_time_hour'] : NULL,
        ];
        $element['settings']['configuration']['week_schedule'][$day_key]['schedule'][$i]['end_time_minute'] = [
          '#type' => 'select',
          '#title' => $this->t('Minute'),
          '#options' => $minute_options,
          '#default_value' => isset($timing[$day_key]['schedule'][$i]['end_time_minute']) ? $timing[$day_key]['schedule'][$i]['end_time_minute'] : NULL,
        ];

      }

      $addMoreName = str_replace('_', '-', $baseName) . '-addmore';
      $element['settings']['configuration']['week_schedule'][$day_key]['schedule']['add'] = [
        '#type' => 'submit',
        '#value' => $this->t('Add'),
        '#submit' => [[get_class($this), 'weekScheduleNewItem']],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => [get_class($this), 'weekScheduleNewItemAjax'],
          'wrapper' => $scheduleId,
        ],
        '#states' => [
          'visible' => [
            ':input[name="' . $this->getCheckBaseNameByElement($element) . '[' . $delta . '][settings][configuration][week_schedule][' . $day_key . '][enable]"]' => ['checked' => TRUE],
          ],
        ],
        '#name' => $addMoreName,
      ];

      $removeItemName = str_replace('_', '-', $baseName) . '-removeitem';
      $element['settings']['configuration']['week_schedule'][$day_key]['schedule']['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#submit' => [[get_class($this), 'weekScheduleRemoveItem']],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => [get_class($this), 'weekScheduleRemoveItemAjax'],
          'wrapper' => $scheduleId,
        ],
        '#states' => [
          'visible' => [
            ':input[name="' . $this->getCheckBaseNameByElement($element) . '[' . $delta . '][settings][configuration][week_schedule][' . $day_key . '][enable]"]' => ['checked' => TRUE],
          ],
        ],
        '#name' => $removeItemName,
      ];
    }

    $element['#attached']['library'][] = 'entity_reservation_system/datepicker_widget';
    $element['#attached']['drupalSettings']['datepicker_widget']['fields'][] = $js_settings;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    parent::massageFormValues($values, $form, $form_state);

    foreach ($values as &$item) {
      $settings = $item['settings'];
      unset($item['settings']);
      $item['status'] = $settings['availability']['status'];
      $item['config'] = $settings['configuration'];
      if ($settings['availability']['status'] === ReservationsInterface::STATUS_SCHEDULED || $settings['availability']['status'] === ReservationsInterface::STATUS_PRIVATE_SCHEDULED) {
        $states = ['open', 'close'];
        foreach ($states as $state) {
          if (!empty($settings['availability']['scheduled'][$state]) && $settings['availability']['scheduled'][$state] instanceof DrupalDateTime) {
            $item[$state] = $settings['availability']['scheduled'][$state]->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
          }
          else {
            $item[$state] = '';
          }
        }
      }
      else {
        $item['open'] = '';
        $item['close'] = '';
      }
      $week_scheduling = $item['config']['week_schedule'];
      foreach ($week_scheduling as $day_key => $day_value) {
        $enabled = $day_value['enable'];
        $schedules = $day_value['schedule'];
        unset($schedules['add']);
        unset($schedules['remove']);
        foreach ($schedules as $delta_schedule => $schedule) {
          if ($enabled &&
            (isset($schedule['start_time_hour']) && is_numeric($schedule['start_time_hour'])) &&
            (isset($schedule['start_time_minute']) && is_numeric($schedule['start_time_minute'])) &&
            (isset($schedule['end_time_hour']) && is_numeric($schedule['end_time_hour'])) &&
            (isset($schedule['end_time_minute']) && is_numeric($schedule['end_time_minute']))
          ) {
            $item['config']['week_schedule'][$day_key]['schedule'][$delta_schedule];
          }
          else {
            unset($item['config']['week_schedule'][$day_key]['schedule'][$delta_schedule]);
          }
        }
        if (empty($schedules)) {
          $item['config']['week_schedule'][$day_key]['enable'] = FALSE;
        }
        if (!$enabled) {
          $item['config']['week_schedule'][$day_key]['schedule'] = [];
        }
      }
    }
    return $values;
  }

  /**
   * Validate callback to ensure that the open date <= the close date.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @see \Drupal\datetime_range\Plugin\Field\FieldWidget\DateRangeWidgetBase::validateOpenClose
   */
  public function validateOpenClose(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $status = $element['status']['#value'];
    if ($status === ReservationsInterface::STATUS_SCHEDULED || $status === ReservationsInterface::STATUS_PRIVATE_SCHEDULED) {
      $open_date = $element['scheduled']['open']['#value']['object'];
      $close_date = $element['scheduled']['close']['#value']['object'];

      // Require open or close dates.
      if (empty($open_date) && empty($close_date)) {
        $form_state->setError($element['scheduled']['open'], $this->t('Please enter an open or close date'));
        $form_state->setError($element['scheduled']['close'], $this->t('Please enter an open or close date'));
      }

      // Make sure open date is not after close date.
      if ($open_date instanceof DrupalDateTime && $close_date instanceof DrupalDateTime) {
        if ($open_date->getTimestamp() !== $close_date->getTimestamp()) {
          $interval = $open_date->diff($close_date);
          if ($interval->invert === 1) {
            $form_state->setError($element['scheduled']['open'], $this->t('The @title close date cannot be before the open date', ['@title' => $element['#title']]));
          }
        }
      }
    }
  }

  /**
   * Validate callback to ensure that the open date <= the close date.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @see \Drupal\datetime_range\Plugin\Field\FieldWidget\DateRangeWidgetBase::validateOpenClose
   */
  public function validateWeekSchedule(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $value = $form_state->getValue($element['#parents']);

    foreach ($value as $day_value) {
      $enabled = $day_value['enable'];
      $schedules = $day_value['schedule'];
      unset($schedules['add']);
      unset($schedules['remove']);
      foreach ($schedules as $schedule) {
        if (
          (!isset($schedule['start_time_hour']) || $schedule['start_time_hour'] == '') ||
          (!isset($schedule['start_time_minute']) || $schedule['start_time_minute'] == '') ||
          (!isset($schedule['end_time_hour']) || $schedule['end_time_hour'] == '') ||
          (!isset($schedule['end_time_minute']) || $schedule['end_time_minute'] == '')
        ) {
          if ($enabled  && !(
            (!isset($schedule['start_time_hour']) || $schedule['start_time_hour'] == '') &&
            (!isset($schedule['start_time_minute']) || $schedule['start_time_minute'] == '') &&
            (!isset($schedule['end_time_hour']) || $schedule['end_time_hour'] == '') &&
            (!isset($schedule['end_time_minute']) || $schedule['end_time_minute'] == '')
          )) {
            $form_state->setError($element, $this->t('You must define all the schedule timings completely'));
          }
        }
        else {
          if (((($schedule['start_time_hour'] * 60) + $schedule['start_time_minute'])) >= (($schedule['end_time_hour'] * 60) + $schedule['end_time_minute'])) {
            $form_state->setError($element, $this->t('The start time must be less than the final time.'));
          }
        }
      }
    }
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public static function weekScheduleNewItem(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $parents = $triggering_element['#parents'];
    $baseName = self::getBaseNameByParents($parents);
    $deltas = $form_state->get($baseName . '_deltas') ? $form_state->get($baseName . '_deltas') : 0;
    if (!empty($deltas)) {
      $deltas++;
    }
    else {
      $deltas = 1;
    }
    $form_state->set($baseName . '_deltas', $deltas);
    $form_state->setRebuild();
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public static function weekScheduleNewItemAjax(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $parents = $triggering_element['#array_parents'];
    array_pop($parents);
    $item = NestedArray::getValue($form, $parents);
    return $item;
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public static function weekScheduleRemoveItem(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $parents = $triggering_element['#parents'];
    $baseName = self::getBaseNameByParents($parents);
    $deltas = $form_state->get($baseName . '_deltas') ? $form_state->get($baseName . '_deltas') : 0;
    if (!empty($deltas)) {
      $deltas--;
    }
    else {
      $deltas = 0;
    }
    $form_state->set($baseName . '_deltas', $deltas);
    $form_state->setRebuild();
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public static function weekScheduleRemoveItemAjax(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $parents = $triggering_element['#array_parents'];
    array_pop($parents);
    $item = NestedArray::getValue($form, $parents);
    return $item;
  }

}
