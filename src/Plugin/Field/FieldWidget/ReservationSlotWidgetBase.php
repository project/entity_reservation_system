<?php

namespace Drupal\entity_reservation_system\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Provides base widget for ReservationSlot field widgets.
 */
abstract class ReservationSlotWidgetBase extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a AddressDefaultWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ModuleHandlerInterface $module_handler) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // @see \Drupal\Core\Field\WidgetPluginManager::createInstance().
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('module_handler')
    );
  }

  /**
   * Return the checkbox base name.
   *
   * @param array $element
   *   The current element.
   *
   * @return string
   *   The base name.
   */
  protected function getCheckBaseNameByElement(array $element) {
    $fieldName = $this->fieldDefinition->getName();
    $nParents = count($element["#field_parents"]);
    if ($nParents > 0) {
      $resp = $element["#field_parents"][0];
      for ($i = 1; $i < $nParents; ++$i) {
        $resp .= '[' . $element["#field_parents"][$i] . ']';
      }
      return $resp . '[' . $fieldName . ']';
    }
    else {
      return $fieldName;
    }
  }

  /**
   * Return the base name calculated by the parents.
   *
   * @param array $parents
   *   The widget parents.
   *
   * @return string
   *   The base name.
   */
  protected static function getBaseNameByParents(array $parents) {
    $offset = count($parents);
    $field = $parents[$offset - 8];
    $delta = $parents[$offset - 7];
    $day_key = $parents[$offset - 3];
    return $field . '_' . $delta . '_' . $day_key;
  }

}
