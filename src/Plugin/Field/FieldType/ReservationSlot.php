<?php

namespace Drupal\entity_reservation_system\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * Plugin implementation of the 'reservation' field type.
 *
 * @FieldType(
 *   id = "reservation_slot",
 *   label = @Translation("Reservations"),
 *   description = @Translation("This field manages configuration and presentation of reservations on an entity."),
 *   default_widget = "reservation_slot_default",
 *   default_formatter = "reservation_slot_default",
 *   cardinality = 1,
 * )
 */
class ReservationSlot extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'status' => [
          'description' => 'Flag to control whether this reservation_slot should be open, closed, or scheduled for new submissions.',
          'type' => 'varchar',
          'length' => 20,
          'not null' => TRUE,
        ],
        'open' => [
          'description' => 'The open date/time.',
          'type' => 'varchar',
          'length' => 20,
          'not null' => FALSE,
        ],
        'close' => [
          'description' => 'The close date/time.',
          'type' => 'varchar',
          'length' => 20,
          'not null' => FALSE,
        ],
        'config' => [
          'type' => 'blob',
          'size' => 'big',
          'serialize' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'status';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['status'] = DataDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Status'))
      ->setDescription(t('Flag to control whether this reservation_slot should be open or closed to new submissions.'));

    $properties['open'] = DataDefinition::create('datetime_iso8601')
      ->setRequired(FALSE)
      ->setLabel(t('Open value'));

    $properties['close'] = DataDefinition::create('datetime_iso8601')
      ->setRequired(FALSE)
      ->setLabel(t('Close value'));

    $properties['config'] = MapDataDefinition::create()->setLabel(t('Configuration'));

    return $properties;
  }

  /**
   * Get all values of the field in an array.
   *
   * @return array
   *   The array with all values.
   */
  public function getPropertyValues() {
    return [
      'status' => $this->status,
      'open' => $this->open,
      'close' => $this->close,
      'config' => $this->config,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if (empty($this->get('status')->getValue())) {
      return TRUE;
    }
    return FALSE;
  }

}
