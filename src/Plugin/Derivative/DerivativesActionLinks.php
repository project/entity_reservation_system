<?php

namespace Drupal\entity_reservation_system\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;

/**
 * Provides dynamic local tasks for units.
 */
class DerivativesActionLinks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The base plugin ID.
   *
   * @var string
   */
  protected $basePluginId;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The redirect destination.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * Constructs a new ContentTranslationLocalTasks.
   *
   * @param string $base_plugin_id
   *   The base plugin ID.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination.
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, RedirectDestinationInterface $redirect_destination) {
    $this->basePluginId = $base_plugin_id;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->redirectDestination = $redirect_destination;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('redirect.destination')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];
    // Create tabs for all possible entity types.
    $fields = $this->entityFieldManager->getFieldMapByFieldType('reservation_slot');
    foreach ($fields as $entity_type_id => $field) {
      // Find the route name for the unit list overview.
      $unit_list_route_base = "entity.$entity_type_id";
      $links[] = [
        'title' => $this->t('Add'),
        'route_name' => $unit_list_route_base . '.reservation_unit_add',
        'appears_on' => [$unit_list_route_base . '.reservation_unit_overview'],
      ] + $base_plugin_definition;

      $links[] = [
        'title' => $this->t('Edit'),
        'route_name' => $unit_list_route_base . '.reservation_unit_edit',
        'appears_on' => [$unit_list_route_base . '.reservation_unit_view'],
      ] + $base_plugin_definition;

      $links[] = [
        'title' => $this->t('Delete'),
        'route_name' => $unit_list_route_base . '.reservation_unit_delete',
        'appears_on' => [$unit_list_route_base . '.reservation_unit_view'],
      ] + $base_plugin_definition;

      $links[] = [
        'title' => $this->t('View'),
        'route_name' => $unit_list_route_base . '.reservation_unit_view',
        'appears_on' => [$unit_list_route_base . '.reservation_unit_edit'],
      ] + $base_plugin_definition;

      $links[] = [
        'title' => $this->t('Delete'),
        'route_name' => $unit_list_route_base . '.reservation_unit_delete',
        'appears_on' => [$unit_list_route_base . '.reservation_unit_edit'],
      ] + $base_plugin_definition;

      $links[] = [
        'title' => $this->t('View'),
        'route_name' => $unit_list_route_base . '.reservation_unit_view',
        'appears_on' => [$unit_list_route_base . '.reservation_unit_delete'],
      ] + $base_plugin_definition;

      $links[] = [
        'title' => $this->t('Edit'),
        'route_name' => $unit_list_route_base . '.reservation_unit_edit',
        'appears_on' => [$unit_list_route_base . '.reservation_unit_delete'],
      ] + $base_plugin_definition;

    }
    return $links;
  }

}
