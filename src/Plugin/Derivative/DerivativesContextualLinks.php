<?php

namespace Drupal\entity_reservation_system\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides dynamic contextual links for units.
 */
class DerivativesContextualLinks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new ContentTranslationContextualLinks.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    // Create tabs for all possible entity types.
    $fields = $this->entityFieldManager->getFieldMapByFieldType('reservation_slot');
    foreach ($fields as $entity_type_id => $field) {
      $this->derivatives[$entity_type_id . '.units']['title'] = $this->t('Units');
      $this->derivatives[$entity_type_id . '.units']['route_name'] = "entity.$entity_type_id.reservation_unit_overview";
      $this->derivatives[$entity_type_id . '.units']['group'] = $entity_type_id;

      $this->derivatives[$entity_type_id . '.slots']['title'] = $this->t('Reservations');
      $this->derivatives[$entity_type_id . '.slots']['route_name'] = "entity.$entity_type_id.reservation_slot_overview";
      $this->derivatives[$entity_type_id . '.slots']['group'] = $entity_type_id;

    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
