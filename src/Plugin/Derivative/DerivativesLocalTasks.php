<?php

namespace Drupal\entity_reservation_system\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides dynamic local tasks for units.
 */
class DerivativesLocalTasks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The base plugin ID.
   *
   * @var string
   */
  protected $basePluginId;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new ContentTranslationLocalTasks.
   *
   * @param string $base_plugin_id
   *   The base plugin ID.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->basePluginId = $base_plugin_id;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    // Create tabs for all possible entity types.
    $fields = $this->entityFieldManager->getFieldMapByFieldType('reservation_slot');
    foreach ($fields as $entity_type_id => $field) {
      $base_route_name = "entity.$entity_type_id.canonical";
      // Find the route name for the unit list overview.
      $unit_list_route_name = "entity.$entity_type_id.reservation_unit_overview";
      $this->derivatives[$unit_list_route_name] = [
        'entity_type' => $entity_type_id,
        'title' => $this->t('Units'),
        'route_name' => $unit_list_route_name,
        'base_route' => $base_route_name,
      ] + $base_plugin_definition;

      $slot_list_route_name = "entity.$entity_type_id.reservation_slot_overview";
      $this->derivatives[$slot_list_route_name] = [
        'entity_type' => $entity_type_id,
        'title' => $this->t('Reservations'),
        'route_name' => $slot_list_route_name,
        'base_route' => $base_route_name,
      ] + $base_plugin_definition;

      $reservation_slot_route_name = "entity.$entity_type_id.reservation_slot_view";
      $this->derivatives[$reservation_slot_route_name] = [
        'entity_type' => 'reservation_slot',
        'title' => $this->t('View'),
        'route_name' => $reservation_slot_route_name,
        'base_route' => $reservation_slot_route_name,
        'weight' => 0,
      ] + $base_plugin_definition;

      $reservation_slot_edit_route_name = "entity.$entity_type_id.reservation_slot_edit";
      $this->derivatives[$reservation_slot_edit_route_name] = [
        'entity_type' => 'reservation_slot',
        'title' => $this->t('Edit'),
        'route_name' => $reservation_slot_edit_route_name,
        'base_route' => $reservation_slot_route_name,
        'weight' => 1,
      ] + $base_plugin_definition;

      $reservation_slot_delete_route_name = "entity.$entity_type_id.reservation_slot_delete";
      $this->derivatives[$reservation_slot_delete_route_name] = [
        'entity_type' => 'reservation_slot',
        'title' => $this->t('Delete'),
        'route_name' => $reservation_slot_delete_route_name,
        'base_route' => $reservation_slot_route_name,
        'weight' => 2,
      ] + $base_plugin_definition;
    }
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
