<?php

namespace Drupal\entity_reservation_system\Plugin\SlotCondition;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Slot condition plugins.
 */
interface SlotConditionInterface extends PluginInspectionInterface {

  /**
   * Evaluate the reservation time based units selection and configuration.
   *
   * @param array $slots
   *   The slots to filter.
   * @param string $day
   *   The day to check.
   * @param array $units
   *   The units to be checked.
   * @param array $config
   *   The field reservation configuration.
   * @param array $context
   *   The field reservation context.
   */
  public function filter(array $slots, $day, array $units, array $config, array $context);

}
