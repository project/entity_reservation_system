<?php

namespace Drupal\entity_reservation_system\Plugin\SlotCondition;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Slot condition plugin manager.
 */
class SlotConditionManager extends DefaultPluginManager {

  /**
   * Constructs a new SlotConditionManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/SlotCondition', $namespaces, $module_handler, 'Drupal\entity_reservation_system\Plugin\SlotCondition\SlotConditionInterface', 'Drupal\entity_reservation_system\Annotation\SlotCondition');
    $this->alterInfo('entity_reservation_system_slot_condition_info');
    $this->setCacheBackend($cache_backend, 'entity_reservation_system_slot_condition_plugins');
  }

  /**
   * Load all definitions.
   *
   * @return \Drupal\entity_reservation_system\Plugin\SlotCondition\SlotConditionInterface[]
   *   An array of Slot Conditions.
   */
  public function loadAllDefinitions() {
    $plugins = $this->getDefinitions();
    $return = [];
    foreach ($plugins as $plugin) {
      $return[$plugin['weight']] = $this->createInstance($plugin['id']);
    }
    ksort($return);
    return $return;
  }

}
