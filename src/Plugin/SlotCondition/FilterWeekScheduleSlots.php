<?php

namespace Drupal\entity_reservation_system\Plugin\SlotCondition;

/**
 * Slot condition to filter out of range of the day slots.
 *
 * @SlotCondition(
 *  id = "filter_week_schedule_slots",
 *  label = @Translation("Filter week schedule slots"),
 *  weight = 8
 * )
 */
class FilterWeekScheduleSlots extends SlotConditionBase {

  /**
   * {@inheritdoc}
   */
  public function filter(array $slots, $day, array $units, array $config, array $context) {
    $day_key = strtolower(date('D', strtotime($day)));
    if ($config['config']['week_schedule'][$day_key]['enable']) {
      $schedules = $config['config']['week_schedule'][$day_key]['schedule'];
      $marks = [];
      foreach ($schedules as $schedule) {
        $start = ($schedule['start_time_hour'] * 60) + $schedule['start_time_minute'];
        $end = ($schedule['end_time_hour'] * 60) + $schedule['end_time_minute'];
        // We must loop every schedule and marks the slots before deleting them
        // because a slot out of one range can be inside one range.
        foreach ($slots as $timeSlot => $slot) {
          if (($timeSlot < $start || $timeSlot >= $end) && empty($marks[$timeSlot])) {
            $marks[$timeSlot] = FALSE;
          }
          else {
            $marks[$timeSlot] = TRUE;
          }
        }
      }
      // Only return the slots with TRUE marks.
      $slots = array_filter(
          $slots,
          function ($key) use ($marks) {
              return $marks[$key];
          },
          ARRAY_FILTER_USE_KEY
      );
    }
    else {
      $slots = [];
    }
    return $slots;
  }

}
