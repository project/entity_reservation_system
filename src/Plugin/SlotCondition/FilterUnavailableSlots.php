<?php

namespace Drupal\entity_reservation_system\Plugin\SlotCondition;

use Drupal\entity_reservation_system\ReservationsInterface;

/**
 * Slot condition to filter already reserved slots.
 *
 * @SlotCondition(
 *  id = "filter_unavailable_slots",
 *  label = @Translation("Filter unavailable slots"),
 *  weight = 10
 * )
 */
class FilterUnavailableSlots extends SlotConditionBase {

  /**
   * {@inheritdoc}
   */
  public function filter(array $slots, $day, array $units, array $config, array $context) {
    // Get all the incompatible units.
    $storage = $this->entityTypeManager->getStorage('reservation_unit');
    $query = $storage->getQuery();
    $query->condition('entity_type', $context['entity_type']);
    $query->condition('entity_id', $context['entity_id']);
    // Only check the desired date.
    $query->condition('incompatible', TRUE);
    $incompatibles = $query->execute();

    // We must take all reservations of one day.
    $storage = $this->entityTypeManager->getStorage('reservation_slot');
    $query = $storage->getQuery();
    $query->condition('entity_type', $context['entity_type']);
    $query->condition('entity_id', $context['entity_id']);
    // Only check the desired date.
    $query->condition('reserved_dates', $day . 'T%', 'LIKE');

    $incompatible = array_intersect(array_keys($units), $incompatibles);
    if (!empty($incompatible)) {
      $query->condition('reservation_units', NULL, 'IS NOT NULL');
    }
    else {
      // Only check the desired units.
      $incompGroup = $query->orConditionGroup();
      $incompGroup->condition('reservation_units', array_keys($units), 'IN');
      if (!empty($incompatibles)) {
        $incompGroup->condition('reservation_units', $incompatibles, 'IN');
      }
      $query->condition($incompGroup);
    }

    // The query has two or conditions, lockeds and completeds or pendings.
    $stateGroup = $query->orConditionGroup();

    // This is the lockeds self condition.
    $lockeds = $query->andConditionGroup();
    $lockeds->condition('status', FALSE);
    $lockeds->condition('reservation_status', ReservationsInterface::RESERVATION_STATUS_LOCKED);

    // This is the completeds or pendings self condition.
    $completeds = $query->andConditionGroup();
    $completeds->condition('status', TRUE);
    $completedsStatues = [
      ReservationsInterface::RESERVATION_STATUS_PENDING,
      ReservationsInterface::RESERVATION_STATUS_CONFIRMED,
    ];
    $completeds->condition('reservation_status', $completedsStatues, 'IN');

    // Adding two and conditions to the or condition.
    $stateGroup->condition($lockeds);
    $stateGroup->condition($completeds);

    // Adding the or condition group to the query.
    $query->condition($stateGroup);

    $result = $query->execute();

    // Load all reservations to check the dates.
    $reservations = $storage->loadMultiple($result);
    $reservedDates = [];
    foreach ($reservations as $reservation) {
      // For each reservation of one day we check that it is valid.
      if (!$reservation->checkExpire()) {
        foreach ($reservation->reserved_dates as $date) {
          $timedate = strtotime($date->value);
          $timeSlot = (date('H', $timedate) * 60) + date('i', $timedate);
          $reservedDates[$timeSlot] = $timedate;
        }
      }
    }
    $slots = array_diff_key($slots, $reservedDates);
    return $slots;
  }

}
