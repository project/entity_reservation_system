<?php

namespace Drupal\entity_reservation_system\Plugin\SlotCondition;

/**
 * Slot condition to filter out of range of the day slots.
 *
 * @SlotCondition(
 *  id = "filter_unavailable_days_slots",
 *  label = @Translation("Filter unavailable days slots"),
 *  weight = 7
 * )
 */
class FilterUnavailableDaysSlots extends SlotConditionBase {

  /**
   * {@inheritdoc}
   */
  public function filter(array $slots, $day, array $units, array $config, array $context) {
    $global_disabled_dates = $this->configFactory->get('entity_reservation_system.settings')->get('global_disabled_dates');
    $disabled_dates = $config['config']['disabled_dates'];

    $disabled = [];
    if (!empty($config['config']['global_disabled_dates'])) {
      $global_disabled_dates = array_filter(explode(';', $global_disabled_dates));
      $disabled_dates = array_filter(explode(';', $disabled_dates));
      $disabled = array_merge($global_disabled_dates, $disabled_dates);
    }
    else {
      $disabled = array_filter(explode(';', $disabled_dates));
    }
    if (in_array($day, $disabled)) {
      $slots = [];
    }
    return $slots;
  }

}
