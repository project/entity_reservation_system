<?php

namespace Drupal\entity_reservation_system\Plugin\SlotCondition;

/**
 * Slot condition to filter out of range of the day slots.
 *
 * @SlotCondition(
 *  id = "filter_out_of_range_slots",
 *  label = @Translation("Filter out or range slots"),
 *  weight = 11
 * )
 */
class FilterOutOfRangeSlots extends SlotConditionBase {

  /**
   * {@inheritdoc}
   */
  public function filter(array $slots, $day, array $units, array $config, array $context) {
    if ($day == date('Y-m-d')) {
      $allow = $this->account->hasPermission("manage reservations for $context[entity_type] $context[bundle]") ? 1 : 0;
      if (!$allow) {
        $date = (date('H') * 60) + date('i');
        $slots = array_filter(
            $slots,
            function ($key) use ($date) {
                return $key > $date;
            },
            ARRAY_FILTER_USE_KEY
        );
      }
      return $slots;
    }
    elseif ($day < date('Y-m-d')) {
      $allow = $this->account->hasPermission("manage reservations for $context[entity_type] $context[bundle]") ? 1 : 0;
      if (!$allow) {
        $slots = [];
      }
    }
    return $slots;
  }

}
