<?php

namespace Drupal\entity_reservation_system;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Reservation slot entity.
 *
 * @see \Drupal\entity_reservation_system\Entity\ReservationSlot.
 */
class ReservationSlotAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\entity_reservation_system\Entity\ReservationSlotInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished reservation slot entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published reservation slot entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit reservation slot entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete reservation slot entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add reservation slot entities');
  }

}
