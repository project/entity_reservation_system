<?php

namespace Drupal\entity_reservation_system;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\entity_reservation_system\Entity\ReservationSlotInterface;

/**
 * Defines the storage handler class for Reservation slot entities.
 *
 * This extends the base storage class, adding required special handling for
 * Reservation slot entities.
 *
 * @ingroup entity_reservation_system
 */
interface ReservationSlotStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Reservation slot revision IDs for a specific Slot.
   *
   * @param \Drupal\entity_reservation_system\Entity\ReservationSlotInterface $entity
   *   The Reservation slot entity.
   *
   * @return int[]
   *   Reservation slot revision IDs (in ascending order).
   */
  public function revisionIds(ReservationSlotInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Reservation slot author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Reservation slot revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\entity_reservation_system\Entity\ReservationSlotInterface $entity
   *   The Reservation slot entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(ReservationSlotInterface $entity);

  /**
   * Unsets the language for all Reservation slot with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
