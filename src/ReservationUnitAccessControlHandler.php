<?php

namespace Drupal\entity_reservation_system;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Reservation unit entity.
 *
 * @see \Drupal\entity_reservation_system\Entity\ReservationUnit.
 */
class ReservationUnitAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\entity_reservation_system\Entity\ReservationUnitInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished reservation unit entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published reservation unit entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit reservation unit entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete reservation unit entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add reservation unit entities');
  }

}
