<?php

namespace Drupal\entity_reservation_system\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\entity_reservation_system\Entity\ReservationUnitInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\entity_reservation_system\Entity\ReservationUnit;

/**
 * Class ReservationUnitController.
 *
 *  Returns responses for Reservation unit routes.
 */
class ReservationUnitController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The Drupal renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Overrides \Drupal\Core\Controller\ControllerBase::__construct().
   *
   * Overrides the construction of context aware to allow for
   * unvalidated constructor based injection of contexts.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The Drupal renderer.
   */
  public function __construct(
    DateFormatterInterface $date_formatter,
    RendererInterface $renderer
  ) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * Displays a Reservation unit  revision.
   *
   * @param int $reservation_unit_revision
   *   The Reservation unit  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($reservation_unit_revision) {
    $reservation_unit = $this->entityManager()->getStorage('reservation_unit')->loadRevision($reservation_unit_revision);
    $view_builder = $this->entityManager()->getViewBuilder('reservation_unit');

    return $view_builder->view($reservation_unit);
  }

  /**
   * Page title callback for a Reservation unit  revision.
   *
   * @param int $reservation_unit_revision
   *   The Reservation unit  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($reservation_unit_revision) {
    $reservation_unit = $this->entityManager()->getStorage('reservation_unit')->loadRevision($reservation_unit_revision);
    return $this->t(
      'Revision of %title from %date', [
        '%title' => $reservation_unit->label(),
        '%date' => $this->dateFormatter->format($reservation_unit->getRevisionCreationTime()),
      ]
    );
  }

  /**
   * Generates an overview table of older revisions of a Reservation unit .
   *
   * @param \Drupal\entity_reservation_system\Entity\ReservationUnitInterface $reservation_unit
   *   A Reservation unit  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(ReservationUnitInterface $reservation_unit) {
    $account = $this->currentUser();
    $langcode = $reservation_unit->language()->getId();
    $langname = $reservation_unit->language()->getName();
    $languages = $reservation_unit->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $reservation_unit_storage = $this->entityManager()->getStorage('reservation_unit');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $reservation_unit->label()]) : $this->t('Revisions for %title', ['%title' => $reservation_unit->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all reservation unit revisions") || $account->hasPermission('administer reservation unit entities')));
    $delete_permission = (($account->hasPermission("delete all reservation unit revisions") || $account->hasPermission('administer reservation unit entities')));

    $rows = [];

    $vids = $reservation_unit_storage->revisionIds($reservation_unit);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\entity_reservation_system\ReservationUnitInterface $revision */
      $revision = $reservation_unit_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $reservation_unit->getRevisionId()) {
          $link = $this->l($date, new Url('entity.reservation_unit.revision', ['reservation_unit' => $reservation_unit->id(), 'reservation_unit_revision' => $vid]));
        }
        else {
          $link = $reservation_unit->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            if ($has_translations) {
              $revert_url = Url::fromRoute(
                'entity.reservation_unit.translation_revert',
                [
                  'reservation_unit' => $reservation_unit->id(),
                  'reservation_unit_revision' => $vid,
                  'langcode' => $langcode,
                ]
              );
            }
            else {
              $revert_url = Url::fromRoute(
                'entity.reservation_unit.revision_revert',
                [
                  'reservation_unit' => $reservation_unit->id(),
                  'reservation_unit_revision' => $vid,
                ]
              );
            }
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $revert_url,
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.reservation_unit.revision_delete', ['reservation_unit' => $reservation_unit->id(), 'reservation_unit_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['reservation_unit_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

  /**
   * Builds the translations overview page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param string $entity_type_id
   *   (optional) The entity type ID.
   *
   * @return array
   *   Array of page elements to render.
   */
  public function overview(RouteMatchInterface $route_match, $entity_type_id = NULL) {
    $hostEntity = $route_match->getparameter($entity_type_id);
    return $this->entityTypeManager()->getListBuilder('reservation_unit')->render(50, $entity_type_id, $hostEntity->id());
  }

  /**
   * Builds an add translation page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object from which to extract the entity type.
   * @param string $entity_type_id
   *   (optional) The entity type ID.
   *
   * @return array
   *   A processed form array ready to be rendered.
   */
  public function add(RouteMatchInterface $route_match, $entity_type_id = NULL) {
    $entity_type = $this->entityTypeManager()->getDefinition($entity_type_id);
    $operation = $entity_type->hasHandlerClass('form', 'add') ? 'add' : 'default';
    $hostEntity = $route_match->getparameter($entity_type_id);
    $unit = ReservationUnit::create([
      'status' => 1,
      'entity_type' => $entity_type_id,
      'entity_id' => $hostEntity->id(),
    ]);
    $form_state_additions = [];
    return $this->entityFormBuilder()->getForm($unit, $operation, $form_state_additions);
  }

  /**
   * Builds the edit translation page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object from which to extract the entity type.
   * @param string $entity_type_id
   *   (optional) The entity type ID.
   *
   * @return array
   *   A processed form array ready to be rendered.
   */
  public function view(RouteMatchInterface $route_match, $entity_type_id = NULL) {
    $view_builder = $this->entityTypeManager()->getViewBuilder('reservation_unit');
    $entity = $route_match->getparameter('reservation_unit');
    $build = $view_builder->view($entity);
    return $build;
  }

  /**
   * Builds the edit translation page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object from which to extract the entity type.
   * @param string $entity_type_id
   *   (optional) The entity type ID.
   *
   * @return array
   *   A processed form array ready to be rendered.
   */
  public function edit(RouteMatchInterface $route_match, $entity_type_id = NULL) {
    $entity_type = $this->entityTypeManager()->getDefinition($entity_type_id);
    $operation = $entity_type->hasHandlerClass('form', 'edit') ? 'edit' : 'default';
    $entity = $route_match->getparameter('reservation_unit');
    $form_state_additions = [];
    return $this->entityFormBuilder()->getForm($entity, $operation, $form_state_additions);
  }

  /**
   * Builds the edit translation page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object from which to extract the entity type.
   * @param string $entity_type_id
   *   (optional) The entity type ID.
   *
   * @return array
   *   A processed form array ready to be rendered.
   */
  public function delete(RouteMatchInterface $route_match, $entity_type_id = NULL) {
    $entity_type = $this->entityTypeManager()->getDefinition($entity_type_id);
    $operation = $entity_type->hasHandlerClass('form', 'delete') ? 'delete' : 'default';
    $entity = $route_match->getparameter('reservation_unit');
    $form_state_additions = [];
    return $this->entityFormBuilder()->getForm($entity, $operation, $form_state_additions);
  }

}
