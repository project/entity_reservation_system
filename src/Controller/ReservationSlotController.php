<?php

namespace Drupal\entity_reservation_system\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\entity_reservation_system\Entity\ReservationSlotInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\entity_reservation_system\Entity\ReservationSlot;

/**
 * Class ReservationSlotController.
 *
 *  Returns responses for Reservation slot routes.
 */
class ReservationSlotController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The Drupal renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Overrides \Drupal\Core\Controller\ControllerBase::__construct().
   *
   * Overrides the construction of context aware to allow for
   * unvalidated constructor based injection of contexts.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The Drupal renderer.
   */
  public function __construct(
    DateFormatterInterface $date_formatter,
    RendererInterface $renderer
  ) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * Displays a Reservation slot  revision.
   *
   * @param int $reservation_slot_revision
   *   The Reservation slot  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($reservation_slot_revision) {
    $reservation_slot = $this->entityManager()->getStorage('reservation_slot')->loadRevision($reservation_slot_revision);
    $view_builder = $this->entityManager()->getViewBuilder('reservation_slot');

    return $view_builder->view($reservation_slot);
  }

  /**
   * Page title callback for a Reservation slot  revision.
   *
   * @param int $reservation_slot_revision
   *   The Reservation slot  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($reservation_slot_revision) {
    $reservation_slot = $this->entityManager()->getStorage('reservation_slot')->loadRevision($reservation_slot_revision);
    return $this->t(
      'Revision of %title from %date',
      [
        '%title' => $reservation_slot->label(),
        '%date' => $this->dateFormatter->format($reservation_slot->getRevisionCreationTime()),
      ]
    );
  }

  /**
   * Generates an overview table of older revisions of a Reservation slot .
   *
   * @param \Drupal\entity_reservation_system\Entity\ReservationSlotInterface $reservation_slot
   *   A Reservation slot  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(ReservationSlotInterface $reservation_slot) {
    $account = $this->currentUser();
    $langcode = $reservation_slot->language()->getId();
    $langname = $reservation_slot->language()->getName();
    $languages = $reservation_slot->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $reservation_slot_storage = $this->entityManager()->getStorage('reservation_slot');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $reservation_slot->label()]) : $this->t('Revisions for %title', ['%title' => $reservation_slot->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all reservation slot revisions") || $account->hasPermission('administer reservation slot entities')));
    $delete_permission = (($account->hasPermission("delete all reservation slot revisions") || $account->hasPermission('administer reservation slot entities')));

    $rows = [];

    $vids = $reservation_slot_storage->revisionIds($reservation_slot);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      $revision = $reservation_slot_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $reservation_slot->getRevisionId()) {
          $link = $this->l($date, new Url('entity.reservation_slot.revision', ['reservation_slot' => $reservation_slot->id(), 'reservation_slot_revision' => $vid]));
        }
        else {
          $link = $reservation_slot->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => Url::fromRoute('entity.reservation_slot.revision_revert', ['reservation_slot' => $reservation_slot->id(), 'reservation_slot_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.reservation_slot.revision_delete', ['reservation_slot' => $reservation_slot->id(), 'reservation_slot_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['reservation_slot_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

  /**
   * Builds the translations overview page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param string $entity_type_id
   *   (optional) The entity type ID.
   *
   * @return array
   *   Array of page elements to render.
   */
  public function overview(RouteMatchInterface $route_match, $entity_type_id = NULL) {
    $entity = $route_match->getparameter($entity_type_id);
    return $this->entityTypeManager()->getListBuilder('reservation_slot')->render(50, $entity_type_id, $entity->id());
  }

  /**
   * Builds an add translation page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object from which to extract the entity type.
   * @param string $entity_type_id
   *   (optional) The entity type ID.
   *
   * @return array
   *   A processed form array ready to be rendered.
   */
  public function add(RouteMatchInterface $route_match, $entity_type_id = NULL) {
    $entity_type = $this->entityTypeManager()->getDefinition($entity_type_id);
    $operation = $entity_type->hasHandlerClass('form', 'add') ? 'add' : 'default';
    $entity = $route_match->getparameter($entity_type_id);
    $slot = ReservationSlot::create([
      'status' => 1,
      'entity_type' => $entity_type_id,
      'entity_id' => $entity->id(),
    ]);
    $form_state_additions = [];
    return $this->entityFormBuilder()->getForm($slot, $operation, $form_state_additions);
  }

  /**
   * Builds the edit translation page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object from which to extract the entity type.
   * @param string $entity_type_id
   *   (optional) The entity type ID.
   *
   * @return array
   *   A processed form array ready to be rendered.
   */
  public function view(RouteMatchInterface $route_match, $entity_type_id = NULL) {
    $view_builder = $this->entityTypeManager()->getViewBuilder('reservation_slot');
    $entity = $route_match->getparameter('reservation_slot');
    $build = $view_builder->view($entity);
    return $build;
  }

  /**
   * Builds the edit translation page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object from which to extract the entity type.
   * @param string $entity_type_id
   *   (optional) The entity type ID.
   *
   * @return array
   *   A processed form array ready to be rendered.
   */
  public function edit(RouteMatchInterface $route_match, $entity_type_id = NULL) {
    $entity_type = $this->entityTypeManager()->getDefinition($entity_type_id);
    $operation = $entity_type->hasHandlerClass('form', 'edit') ? 'edit' : 'default';
    $entity = $route_match->getparameter('reservation_slot');
    $form_state_additions = [];
    return $this->entityFormBuilder()->getForm($entity, $operation, $form_state_additions);
  }

  /**
   * Builds the edit translation page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object from which to extract the entity type.
   * @param string $entity_type_id
   *   (optional) The entity type ID.
   *
   * @return array
   *   A processed form array ready to be rendered.
   */
  public function delete(RouteMatchInterface $route_match, $entity_type_id = NULL) {
    $entity_type = $this->entityTypeManager()->getDefinition($entity_type_id);
    $operation = $entity_type->hasHandlerClass('form', 'delete') ? 'delete' : 'default';
    $entity = $route_match->getparameter('reservation_slot');
    $form_state_additions = [];
    return $this->entityFormBuilder()->getForm($entity, $operation, $form_state_additions);
  }

}
