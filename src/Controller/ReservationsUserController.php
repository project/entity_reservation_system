<?php

namespace Drupal\entity_reservation_system\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\entity_reservation_system\Entity\ReservationSlotInterface;
use Drupal\entity_reservation_system\ReservationsInterface;
use Drupal\user\UserInterface;

/**
 * Base class for entity translation controllers.
 */
class ReservationsUserController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The redirect destination.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * Initializes a content translation controller.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, RedirectDestinationInterface $redirect_destination) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->redirectDestination = $redirect_destination;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('redirect.destination')
    );
  }

  /**
   * Builds the translations overview page.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user account.
   *
   * @return array
   *   Array of page elements to render.
   */
  public function overview(UserInterface $user) {
    return $this->entityTypeManager()->getListBuilder('reservation_slot')->render(50, NULL, NULL, NULL, NULL, $user->id());
  }

  /**
   * Builds an add translation page.
   *
   * @param string $entity_type
   *   The entity_type of the reservation..
   * @param string $bundle
   *   The bundle of the reservation..
   * @param string $entity_id
   *   The entity_id of the reservation..
   * @param string $field_name
   *   The field_name of the reservation..
   * @param string $delta
   *   The delta of the reservation..
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The route match object from which to extract the entity type.
   *
   * @return array
   *   A processed form array ready to be rendered.
   */
  public function add($entity_type, $bundle, $entity_id, $field_name, $delta, Request $request) {
    $form = 'Drupal\entity_reservation_system\Form\CreateReservationForm';
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
    $config = $entity->{$field_name}->first()->getPropertyValues();
    $entityArray = [
      'status' => 0,
      'uid' => $this->currentUser()->id(),
      'reservation_status' => ReservationsInterface::RESERVATION_STATUS_LOCKED,
      'entity_type' => isset($entity_type) ? $entity_type : NULL,
      'entity_id' => isset($entity_id) ? $entity_id : NULL,
      'field_name' => isset($field_name) ? $field_name : NULL,
      'delta' => isset($delta) ? $delta : NULL,
    ];
    $context = [
      'entity_type' => isset($entity_type) ? $entity_type : NULL,
      'bundle' => isset($bundle) ? $bundle : NULL,
      'entity_id' => isset($entity_id) ? $entity_id : NULL,
      'field_name' => isset($field_name) ? $field_name : NULL,
      'delta' => isset($delta) ? $delta : NULL,
    ];
    return $this->formBuilder()->getForm($form, $entityArray, $config, $context);
  }

  /**
   * Builds the edit translation page.
   *
   * @param \Drupal\entity_reservation_system\Entity\ReservationSlotInterface $reservation_slot
   *   The reservation slot..
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The route match object from which to extract the entity type.
   *
   * @return array
   *   A processed form array ready to be rendered.
   */
  public function view(ReservationSlotInterface $reservation_slot, Request $request) {
    $view_builder = $this->entityTypeManager->getViewBuilder('reservation_slot');
    $build = $view_builder->view($reservation_slot);
    return $build;
  }

  /**
   * Builds the edit translation page.
   *
   * @param \Drupal\entity_reservation_system\Entity\ReservationSlotInterface $reservation_slot
   *   The reservation slot..
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The route match object from which to extract the entity type.
   *
   * @return array
   *   A processed form array ready to be rendered.
   */
  public function edit(ReservationSlotInterface $reservation_slot, Request $request) {
    $form = 'Drupal\entity_reservation_system\Form\EditReservationForm';
    return $this->formBuilder()->getForm($form, $reservation_slot);
  }

  /**
   * Builds the cancel translation page.
   *
   * @param \Drupal\entity_reservation_system\Entity\ReservationSlotInterface $reservation_slot
   *   The reservation slot..
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The route match object from which to extract the entity type.
   *
   * @return array
   *   A processed form array ready to be rendered.
   */
  public function cancel(ReservationSlotInterface $reservation_slot, Request $request) {
    $form = 'Drupal\entity_reservation_system\Form\CancelReservationForm';
    return $this->formBuilder()->getForm($form, $reservation_slot);
  }

  /**
   * Builds the cancel translation page.
   *
   * @param \Drupal\entity_reservation_system\Entity\ReservationSlotInterface $reservation_slot
   *   The reservation slot..
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The route match object from which to extract the entity type.
   *
   * @return array
   *   A processed form array ready to be rendered.
   */
  public function contact(ReservationSlotInterface $reservation_slot, Request $request) {
    $form = 'Drupal\entity_reservation_system\Form\ContactReservationForm';
    return $this->formBuilder()->getForm($form, $reservation_slot);
  }

}
