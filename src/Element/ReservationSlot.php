<?php

namespace Drupal\entity_reservation_system\Element;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\entity_reservation_system\ReservationsInterface;

/**
 * Provides a render element to display a reservation slot.
 *
 * @RenderElement("reservation_slot")
 */
class ReservationSlot extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#pre_render' => [
        [$class, 'preRenderReservationSlotElement'],
      ],
      '#entity' => NULL,
      '#configuration' => [],
      '#field_name' => NULL,
      '#delta' => NULL,
      '#operation' => 'create',
    ];
  }

  /**
   * Webform element pre render callback.
   */
  public static function preRenderReservationSlotElement($element) {
    $config = [];
    if (!empty($element['#configuration'])) {
      $config = $element['#configuration'];
      if (
        (
          $config['status'] == ReservationsInterface::STATUS_PRIVATE ||
          $config['status'] == ReservationsInterface::STATUS_PRIVATE_SCHEDULED
        ) &&
        !\Drupal::currentUser()->hasPermission('add private reservation slot')
      ) {
        return $element;
      }
      elseif ($config['status'] == ReservationsInterface::STATUS_CLOSED) {
        return $element;
      }
    }
    $values = [];

    // Set source entity type and id.
    if (isset($element['#entity']) && $element['#entity'] instanceof EntityInterface) {
      $values['entity_type'] = $element['#entity']->getEntityTypeId();
      $values['bundle'] = $element['#entity']->bundle();
      $values['entity_id'] = $element['#entity']->id();
    }

    if (isset($values['entity_type']) && isset($values['entity_id'])) {
      $entityArray = [
        'status' => 0,
        'uid' => \Drupal::currentUser()->id(),
        'reservation_status' => 'locked',
        'entity_type' => isset($values['entity_type']) ? $values['entity_type'] : NULL,
        'entity_id' => isset($values['entity_id']) ? $values['entity_id'] : NULL,
        'field_name' => isset($element['#field_name']) ? $element['#field_name'] : NULL,
        'delta' => isset($element['#delta']) ? $element['#delta'] : NULL,
      ];
      $context = [
        'entity_type' => isset($values['entity_type']) ? $values['entity_type'] : NULL,
        'bundle' => isset($values['bundle']) ? $values['bundle'] : NULL,
        'entity_id' => isset($values['entity_id']) ? $values['entity_id'] : NULL,
        'field_name' => isset($element['#field_name']) ? $element['#field_name'] : NULL,
        'delta' => isset($element['#delta']) ? $element['#delta'] : NULL,
      ];
      switch ($element['#operation']) {
        case 'create':
          if (\Drupal::currentUser()->hasPermission('add reservation slot') || \Drupal::currentUser()->hasPermission('add private reservation slot')) {
            // Build the reservation form.
            $element['reservation_slot_build'] = \Drupal::service('form_builder')->getForm('Drupal\entity_reservation_system\Form\CreateReservationForm', $entityArray, $config, $context);
          }
          else {
            // Set access denied message.
            $element['acces_denied_message'] = static::buildErrorMessage(t("You don't have permissions to access this reservation form."));
          }
          break;

        case 'edit':
          if (\Drupal::currentUser()->hasPermission('edit reservation slot')) {
            // Build the reservation form.
            $element['reservation_slot_build'] = \Drupal::service('form_builder')->getForm('Drupal\entity_reservation_system\Form\CreateReservationForm', $entityArray, $config, $context);
          }
          else {
            // Set access denied message.
            $element['acces_denied_message'] = static::buildErrorMessage(t("You don't have permissions to access this reservation form."));
          }
          break;

        case 'remove':
          if (\Drupal::currentUser()->hasPermission('delete reservation slot')) {
            // Build the reservation form.
            $element['reservation_slot_build'] = \Drupal::service('form_builder')->getForm('Drupal\entity_reservation_system\Form\CreateReservationForm', $entityArray, $config, $context);
          }
          else {
            // Set access denied message.
            $element['acces_denied_message'] = static::buildErrorMessage(t("You don't have permissions to access this reservation form."));
          }
          break;
      }
    }
    else {
      // Set access denied message.
      $element['acces_denied_message'] = static::buildErrorMessage(t("The host entity is mandatory."));
    }
    return $element;
  }

  /**
   * Build access denied message for a reservation.
   *
   * @param string $message
   *   The message to show to user.
   *
   * @return array
   *   A renderable array containing thea ccess denied message.
   */
  public static function buildErrorMessage($message) {

    $attributes = [];
    $attributes['class'][] = 'reservation-error-message';
    $build = [
      '#type' => 'container',
      '#attributes' => $attributes,
      'message' => [
        '#markup' => $message,
      ],
    ];
    return $build;
  }

}
