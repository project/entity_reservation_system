<?php

namespace Drupal\entity_reservation_system\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Slot condition item annotation object.
 *
 * @see \Drupal\entity_reservation_system\Plugin\SlotCondition\SlotConditionManager
 * @see plugin_api
 *
 * @Annotation
 */
class SlotCondition extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The plugin weight.
   *
   * @var int
   */
  public $weight;

}
