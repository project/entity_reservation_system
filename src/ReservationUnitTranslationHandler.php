<?php

namespace Drupal\entity_reservation_system;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for reservation_unit.
 */
class ReservationUnitTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
