<?php

namespace Drupal\entity_reservation_system;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Defines a class to build a listing of Reservation unit entities.
 *
 * @ingroup entity_reservation_system
 */
class ReservationUnitListBuilder extends EntityListBuilder implements FormInterface {

  use RedirectDestinationTrait;

  /**
   * A instance of entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  protected $limit = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $hostEntityType = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $hostEntityId = FALSE;

  /**
   * Constructs a new BlockListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    FormBuilderInterface $form_builder,
    Request $request,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($entity_type, $storage);
    $this->formBuilder = $form_builder;
    $this->request = $request;
    $this->routeMatch = $route_match;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager')->getStorage($entity_type->id()),
      $container->get('form_builder'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reservation_unit_list_builder';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['filters'] = [
      '#type' => 'container',
      '#weight' => 1,
      '#attributes' => [
        'class' => ['container-inline'],
      ],
    ];
    $name = $this->request->query->get('name');
    $form['filters']['name'] = [
      '#type' => 'textfield',
      '#default_value' => $name,
      '#title' => $this->t('Name'),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Filter'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();
    if (!empty($this->hostEntityType)) {
      $hostEntity = $this->routeMatch->getParameter($this->hostEntityType);
      $url = Url::fromRoute($this->routeMatch->getRouteName(), [$this->hostEntityType => $hostEntity->id()], ['query' => $form_state->getValues()]);
      $form_state->setRedirectUrl($url);
    }
    else {
      $form_state->setRedirect($this->routeMatch->getRouteName(), $form_state->getValues());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entity_query = $this->storage->getQuery();
    $name = $this->request->query->get('name');
    if (!empty($name)) {
      $entity_query->condition('name', '%' . Database::getConnection()->escapeLike($name) . '%', 'LIKE');
    }
    if (!empty($this->hostEntityType)) {
      $entity_query->condition('entity_type', $this->hostEntityType);
    }
    if (!empty($this->hostEntityId)) {
      $entity_query->condition('entity_id', $this->hostEntityId);
    }
    $header = $this->buildHeader();
    $entity_query->pager($this->limit);
    $entity_query->tableSort($header);
    $uids = $entity_query->execute();
    return $this->storage->loadMultiple($uids);
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = [
      'data' => $this->t('Code'),
      'field' => 'name',
      'specifier' => 'name',
      'sort' => 'ASC',
    ];
    $header['status'] = [
      'data' => $this->t('Status'),
      'field' => 'status',
      'specifier' => 'status',
      'sort' => 'ASC',
    ];
    $header['changed'] = [
      'data' => $this->t('Changed'),
      'field' => 'changed',
      'sort' => 'DESC',
      'specifier' => 'changed',
    ];
    $header['operations'] = [
      'data' => $this->t('Operations'),
    ];
    $header = $header + parent::buildHeader();
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    if (!empty($this->hostEntityType)) {
      $hostEntity = $this->entityTypeManager->getStorage($entity->getHostEntityType())->load($entity->getHostEntityId());
      $row['name'] = Link::fromTextAndUrl(
        $entity->getName(),
        $hostEntity->toUrl('drupal:reservation-unit-view')->setRouteParameter('reservation_unit', $entity->id())
      );
    }
    else {
      $row['name'] = Link::fromTextAndUrl(
        $entity->getName(),
        $entity->toUrl()
      );
    }
    $row['status'] = $entity->isPublished() ? $this->t('Actived') : $this->t('Disabled');
    $row['changed'] = $this->dateFormatter->format($entity->changed->value, 'short');
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   *
   * Builds the entity listing as renderable array for table.html.twig.
   *
   * @todo Add a link to add a new item to the #empty name.
   */
  public function render($limit = 50, $entity_type = NULL, $entity_id = NULL) {
    $this->limit = $limit;
    $this->hostEntityType = $entity_type;
    $this->hostEntityId = $entity_id;

    $build['#title'] = $this->t('Units');

    $build['form'] = [
      $this->formBuilder()->getForm($this),
    ];

    $build['table'] = [
      '#type' => 'table',
      '#title' => $this->getTitle(),
      '#rows' => [],
      '#header' => $this->buildHeader(),
      '#empty' => $this->t('There is no @label yet.', ['@label' => $this->entityType->getLabel()]),
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
    ];
    foreach ($this->load() as $entity) {
      if ($row = $this->buildRow($entity)) {
        $build['table']['#rows'][$entity->id()] = $row;
      }
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }
    return $build;
  }

  /**
   * Returns the form builder.
   *
   * @return \Drupal\Core\Form\FormBuilderInterface
   *   The form builder.
   */
  protected function formBuilder() {
    if (!$this->formBuilder) {
      $this->formBuilder = \Drupal::formBuilder();
    }
    return $this->formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = [];
    $hostEntity = $this->entityTypeManager->getStorage($entity->getHostEntityType())->load($entity->getHostEntityId());
    if ($entity->access('update') && $hostEntity->hasLinkTemplate('drupal:reservation-unit-edit')) {
      $operations['edit'] = [
        'title' => $this->t('Edit'),
        'weight' => 10,
        'url' => $this->ensureDestination($hostEntity->toUrl('drupal:reservation-unit-edit')->setRouteParameter('reservation_unit', $entity->id())),
      ];
    }
    if ($entity->access('delete') && $hostEntity->hasLinkTemplate('drupal:reservation-unit-delete')) {
      $operations['delete'] = [
        'title' => $this->t('Delete'),
        'weight' => 100,
        'url' => $this->ensureDestination($hostEntity->toUrl('drupal:reservation-unit-delete')->setRouteParameter('reservation_unit', $entity->id())),
      ];
    }
    $operations += $this->moduleHandler()->invokeAll('entity_operation', [$entity]);
    $this->moduleHandler->alter('entity_operation', $operations, $entity);
    uasort($operations, '\Drupal\Component\Utility\SortArray::sortByWeightElement');
    return $operations;
  }

}
