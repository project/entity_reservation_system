<?php

namespace Drupal\entity_reservation_system\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Subscriber for entity translation routes.
 */
class EntityReservationSystemRouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new ContentTranslationContextualLinks.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $fields = $this->entityFieldManager->getFieldMapByFieldType('reservation_slot');
    foreach ($fields as $entity_type_id => $field) {
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id, FALSE);
      // Inherit admin route status from edit route, if exists.
      $is_admin = FALSE;
      $route_name = "entity.$entity_type_id.edit_form";
      if ($edit_route = $collection->get($route_name)) {
        $is_admin = (bool) $edit_route->getOption('_admin_route');
      }

      // Units routes.
      if ($entity_type->hasLinkTemplate('drupal:reservation-unit-overview')) {
        $route = new Route(
          $entity_type->getLinkTemplate('drupal:reservation-unit-overview'),
          [
            '_controller' => '\Drupal\entity_reservation_system\Controller\ReservationUnitController::overview',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_entity_access' => $entity_type_id . '.view',
            '_access_entity_units_manage_access' => $entity_type_id,
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $route_name = "entity.$entity_type_id.reservation_unit_overview";
        $collection->add($route_name, $route);
      }

      if ($entity_type->hasLinkTemplate('drupal:reservation-unit-add')) {
        $route = new Route(
          $entity_type->getLinkTemplate('drupal:reservation-unit-add'),
          [
            '_controller' => '\Drupal\entity_reservation_system\Controller\ReservationUnitController::add',
            '_title' => 'Add',
            'entity_type_id' => $entity_type_id,

          ],
          [
            '_entity_access' => $entity_type_id . '.view',
            '_access_entity_units_manage_access' => $entity_type_id,
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $collection->add("entity.$entity_type_id.reservation_unit_add", $route);
      }

      if ($entity_type->hasLinkTemplate('drupal:reservation-unit-view')) {
        $route = new Route(
          $entity_type->getLinkTemplate('drupal:reservation-unit-view'),
          [
            '_controller' => '\Drupal\entity_reservation_system\Controller\ReservationUnitController::view',
            'id' => NULL,
            '_title' => 'View',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_access_entity_units_manage_access' => $entity_type_id,
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
              'reservation_unit' => [
                'type' => 'entity:reservation_unit',
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $collection->add("entity.$entity_type_id.reservation_unit_view", $route);
      }

      if ($entity_type->hasLinkTemplate('drupal:reservation-unit-edit')) {
        $route = new Route(
          $entity_type->getLinkTemplate('drupal:reservation-unit-edit'),
          [
            '_controller' => '\Drupal\entity_reservation_system\Controller\ReservationUnitController::edit',
            'id' => NULL,
            '_title' => 'Edit',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_access_entity_units_manage_access' => $entity_type_id,
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
              'reservation_unit' => [
                'type' => 'entity:reservation_unit',
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $collection->add("entity.$entity_type_id.reservation_unit_edit", $route);
      }

      if ($entity_type->hasLinkTemplate('drupal:reservation-unit-delete')) {
        $route = new Route(
          $entity_type->getLinkTemplate('drupal:reservation-unit-delete'),
          [
            '_controller' => '\Drupal\entity_reservation_system\Controller\ReservationUnitController::delete',
            'id' => NULL,
            '_title' => 'Delete',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_access_entity_units_manage_access' => $entity_type_id,
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
              'reservation_unit' => [
                'type' => 'entity:reservation_unit',
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $collection->add("entity.$entity_type_id.reservation_unit_delete", $route);
      }

      // Reservations routes.
      if ($entity_type->hasLinkTemplate('drupal:reservation-slot-overview')) {
        $route = new Route(
          $entity_type->getLinkTemplate('drupal:reservation-slot-overview'),
          [
            '_controller' => '\Drupal\entity_reservation_system\Controller\ReservationSlotController::overview',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_entity_access' => $entity_type_id . '.view',
            '_access_entity_slots_manage_access' => $entity_type_id,
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $route_name = "entity.$entity_type_id.reservation_slot_overview";
        $collection->add($route_name, $route);
      }

      if ($entity_type->hasLinkTemplate('drupal:reservation-slot-add')) {
        $route = new Route(
          $entity_type->getLinkTemplate('drupal:reservation-slot-add'),
          [
            '_controller' => '\Drupal\entity_reservation_system\Controller\ReservationSlotController::add',
            '_title' => 'Add',
            'entity_type_id' => $entity_type_id,

          ],
          [
            '_entity_access' => $entity_type_id . '.view',
            '_access_entity_slots_manage_access' => $entity_type_id,
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $collection->add("entity.$entity_type_id.reservation_slot_add", $route);
      }

      if ($entity_type->hasLinkTemplate('drupal:reservation-slot-view')) {
        $route = new Route(
          $entity_type->getLinkTemplate('drupal:reservation-slot-view'),
          [
            '_controller' => '\Drupal\entity_reservation_system\Controller\ReservationSlotController::view',
            'id' => NULL,
            '_title' => 'Reservation',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_access_entity_slots_manage_access' => $entity_type_id,
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
              'reservation_slot' => [
                'type' => 'entity:reservation_slot',
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $collection->add("entity.$entity_type_id.reservation_slot_view", $route);
      }

      if ($entity_type->hasLinkTemplate('drupal:reservation-slot-edit')) {
        $route = new Route(
          $entity_type->getLinkTemplate('drupal:reservation-slot-edit'),
          [
            '_controller' => '\Drupal\entity_reservation_system\Controller\ReservationSlotController::edit',
            'id' => NULL,
            '_title' => 'Edit',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_access_entity_slots_manage_access' => $entity_type_id,
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
              'reservation_slot' => [
                'type' => 'entity:reservation_slot',
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $collection->add("entity.$entity_type_id.reservation_slot_edit", $route);
      }

      if ($entity_type->hasLinkTemplate('drupal:reservation-slot-delete')) {
        $route = new Route(
          $entity_type->getLinkTemplate('drupal:reservation-slot-delete'),
          [
            '_controller' => '\Drupal\entity_reservation_system\Controller\ReservationSlotController::delete',
            'id' => NULL,
            '_title' => 'Delete',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_access_entity_slots_manage_access' => $entity_type_id,
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
              'reservation_slot' => [
                'type' => 'entity:reservation_slot',
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $collection->add("entity.$entity_type_id.reservation_slot_delete", $route);
      }

    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = parent::getSubscribedEvents();
    // Should run after AdminRouteSubscriber so the routes can inherit admin
    // status of the edit routes on entities. Therefore priority -210.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -210];
    return $events;
  }

}
