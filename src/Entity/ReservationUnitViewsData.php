<?php

namespace Drupal\entity_reservation_system\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Reservation unit entities.
 */
class ReservationUnitViewsData extends EntityViewsData {
}
