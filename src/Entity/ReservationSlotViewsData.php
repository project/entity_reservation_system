<?php

namespace Drupal\entity_reservation_system\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Reservation slot entities.
 */
class ReservationSlotViewsData extends EntityViewsData {
}
