<?php

namespace Drupal\entity_reservation_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Reservation unit entities.
 *
 * @ingroup entity_reservation_system
 */
interface ReservationUnitInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Reservation unit name.
   *
   * @return string
   *   Name of the Reservation unit.
   */
  public function getName();

  /**
   * Sets the Reservation unit name.
   *
   * @param string $name
   *   The Reservation unit name.
   *
   * @return \Drupal\entity_reservation_system\Entity\ReservationUnitInterface
   *   The called Reservation unit entity.
   */
  public function setName($name);

  /**
   * Gets the Reservation unit creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Reservation unit.
   */
  public function getCreatedTime();

  /**
   * Sets the Reservation unit creation timestamp.
   *
   * @param int $timestamp
   *   The Reservation unit creation timestamp.
   *
   * @return \Drupal\entity_reservation_system\Entity\ReservationUnitInterface
   *   The called Reservation unit entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Reservation unit published status indicator.
   *
   * Unpublished Reservation unit are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Reservation unit is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Reservation unit.
   *
   * @param bool $published
   *   TRUE to set this Reservation unit to published, FALSE to set it to
   *   unpublished.
   *
   * @return \Drupal\entity_reservation_system\Entity\ReservationUnitInterface
   *   The called Reservation unit entity.
   */
  public function setPublished($published);

  /**
   * Returns the Reservation unit incompatible indicator.
   *
   * @return bool
   *   TRUE if the Reservation unit is incompatible.
   */
  public function isIncompatible();

  /**
   * Sets the Reservation unit incompatible indicator.
   *
   * @param bool $incompatible
   *   TRUE to set this Reservation unit to incompatible, FALSE elsewhere.
   *
   * @return \Drupal\entity_reservation_system\Entity\ReservationUnitInterface
   *   The called Reservation unit entity.
   */
  public function setIncompatible($incompatible);

  /**
   * Gets the Reservation unit revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Reservation unit revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\entity_reservation_system\Entity\ReservationUnitInterface
   *   The called Reservation unit entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Reservation unit revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Reservation unit revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\entity_reservation_system\Entity\ReservationUnitInterface
   *   The called Reservation unit entity.
   */
  public function setRevisionUserId($uid);

}
