<?php

namespace Drupal\entity_reservation_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Hashids\Hashids;
use Drupal\entity_reservation_system\ReservationsInterface;

/**
 * Defines the Reservation slot entity.
 *
 * @ingroup entity_reservation_system
 *
 * @ContentEntityType(
 *   id = "reservation_slot",
 *   label = @Translation("Reservation slot"),
 *   handlers = {
 *     "storage" = "Drupal\entity_reservation_system\ReservationSlotStorage",
 *     "view_builder" = "Drupal\entity_reservation_system\ReservationViewBuilder",
 *     "list_builder" = "Drupal\entity_reservation_system\ReservationSlotListBuilder",
 *     "views_data" = "Drupal\entity_reservation_system\Entity\ReservationSlotViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\entity_reservation_system\Form\ReservationSlotForm",
 *       "edit" = "Drupal\entity_reservation_system\Form\ReservationSlotForm",
 *       "delete" = "Drupal\entity_reservation_system\Form\ReservationSlotDeleteForm",
 *     },
 *     "access" = "Drupal\entity_reservation_system\ReservationSlotAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\entity_reservation_system\ReservationSlotHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "reservation_slot",
 *   revision_table = "reservation_slot_revision",
 *   revision_data_table = "reservation_slot_field_revision",
 *   admin_permission = "administer reservation slot entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "code",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/reservations/reservation/{reservation_slot}",
 *     "edit-form" = "/admin/reservations/reservation/{reservation_slot}/edit",
 *     "delete-form" = "/admin/reservations/reservation/{reservation_slot}/delete",
 *     "version-history" = "/admin/reservations/reservation/{reservation_slot}/revisions",
 *     "revision" = "/admin/reservations/reservation/{reservation_slot}/revisions/{reservation_slot_revision}/view",
 *     "revision_revert" = "/admin/reservations/reservation/{reservation_slot}/revisions/{reservation_slot_revision}/revert",
 *     "revision_delete" = "/admin/reservations/reservation/{reservation_slot}/revisions/{reservation_slot_revision}/delete",
 *     "collection" = "/admin/reservations/reservation-list",
 *   },
 *   field_ui_base_route = "entity.reservation_slot.collection"
 * )
 */
class ReservationSlot extends RevisionableContentEntityBase implements ReservationSlotInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the reservation_slot
    // owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }

    $notification_statues = [
      ReservationsInterface::RESERVATION_STATUS_PENDING,
      ReservationsInterface::RESERVATION_STATUS_CONFIRMED,
      ReservationsInterface::RESERVATION_STATUS_CANCELLED,
    ];
    if (in_array($this->getReservationStatus(), $notification_statues) && $this->isPublished()) {
      if (!$update) {
        $system_site_config = \Drupal::config('system.site');
        $site_email = $system_site_config->get('mail');
        $message = $this->entityTypeManager()->getStorage('message')->create([
          'template' => 'reservation_created',
          'field_reservation' => $this->id(),
          'uid' => $this->getOwnerId(),
        ]);
        $message->save();
        if ($message) {
          \Drupal::service('message_notify.sender')->send($message, []);
          \Drupal::service('message_notify.sender')->send($message, ['mail' => $site_email]);
        }
      }
      else {
        $system_site_config = \Drupal::config('system.site');
        $site_email = $system_site_config->get('mail');
        $message = $this->entityTypeManager()->getStorage('message')->create([
          'template' => 'reservation_modified',
          'field_reservation' => $this->id(),
          'uid' => $this->getOwnerId(),
        ]);
        $message->save();
        if ($message) {
          \Drupal::service('message_notify.sender')->send($message, []);
          \Drupal::service('message_notify.sender')->send($message, ['mail' => $site_email]);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCode() {
    return $this->get('code')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCode($code) {
    $this->set('code', $code);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHostEntityType() {
    return $this->get('entity_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHostEntityType($entity_type) {
    $this->set('entity_type', $entity_type);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHostEntityId() {
    return $this->get('entity_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHostEntityId($entity_id) {
    $this->set('entity_id', $entity_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHostFieldName() {
    return $this->get('field_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHostFieldName($field_name) {
    $this->set('field_name', $field_name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHostFieldDelta() {
    return $this->get('delta')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHostFieldDelta($delta) {
    $this->set('delta', $delta);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    $this->set('reservation_status', $published ? 'pending' : 'locked');
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getReservationStatus() {
    return $this->get('reservation_status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setReservationStatus($status) {
    if ($this->getReservationStatus() == 'locked' && $status != 'expired' && $this->getReservationStatus() != $status) {
      // Create the hash class with length of 8 digits.
      $hashids = new Hashids('', 8);
      $code = $hashids->encode(time());
      $this->setCode($code);
    }
    $this->set('reservation_status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getReservationExpire() {
    return $this->get('expire')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setReservationExpire($expire) {
    $this->set('expire', $expire);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function checkExpire() {
    if ($this->getReservationStatus() == ReservationsInterface::RESERVATION_STATUS_LOCKED && $this->getReservationExpire() <= time() && !$this->isPublished()) {
      $this->setReservationStatus('expired');
      $this->save();
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the reservation slot entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('code'))
      ->setDescription(t('The code of the Reservation slot entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Reservation slot is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 2,
      ]);

    $fields['reservation_status'] = BaseFieldDefinition::create("list_string")
      ->setSettings([
        'allowed_values' => [
          ReservationsInterface::RESERVATION_STATUS_LOCKED => ReservationsInterface::RESERVATION_STATUS_LOCKED_LABEL,
          ReservationsInterface::RESERVATION_STATUS_EXPIRED => ReservationsInterface::RESERVATION_STATUS_EXPIRED_LABEL,
          ReservationsInterface::RESERVATION_STATUS_PENDING => ReservationsInterface::RESERVATION_STATUS_PENDING_LABEL,
          ReservationsInterface::RESERVATION_STATUS_CONFIRMED => ReservationsInterface::RESERVATION_STATUS_CONFIRMED_LABEL,
          ReservationsInterface::RESERVATION_STATUS_CANCELLED => ReservationsInterface::RESERVATION_STATUS_CANCELLED_LABEL,
        ],
      ])
      ->setDefaultValue('pending')
      ->setLabel(t('Reservation status'))
      ->setDescription(t('State of the reservation'))
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['expire'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Expiration'))
      ->setDescription(t('The time that the entity expire if the reservation is not reserved.'));

    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity type'))
      ->setDescription(t('The entity type to which this unit was attached.'))
      ->setSetting('is_ascii', TRUE)
      ->setSetting('max_length', EntityTypeInterface::ID_MAX_LENGTH);

    // Not limited to integers to prevent references to another entities without
    // numeric ids.
    $fields['entity_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity ID of the host entity'))
      ->setDescription(t('The ID of the entity of which this unit was attached.'))
      ->setSetting('max_length', 255);

    $fields['field_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Field name'))
      ->setDescription(t('The name of the field that stores the reservation configuration.'))
      ->setSetting('is_ascii', TRUE);

    // Not limited to integers to prevent references to another entities without
    // numeric ids.
    $fields['delta'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Field delta'))
      ->setDescription(t('The delta of the field that stores the reservation configuration.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDefaultEntityOwner() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseConfig() {
    $entityTypeManager = $this->entityTypeManager();
    $hostEntity = $entityTypeManager->getStorage($this->getHostEntityType())->load($this->getHostEntityId());
    $fieldName = $this->getHostFieldName();
    $delta = $this->getHostFieldDelta();
    return $hostEntity->{$fieldName}[$delta]->getPropertyValues();
  }

}
