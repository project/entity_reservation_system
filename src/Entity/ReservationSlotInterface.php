<?php

namespace Drupal\entity_reservation_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Reservation slot entities.
 *
 * @ingroup entity_reservation_system
 */
interface ReservationSlotInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Reservation slot name.
   *
   * @return string
   *   Name of the Reservation slot.
   */
  public function getCode();

  /**
   * Sets the Reservation slot name.
   *
   * @param string $code
   *   The Reservation slot code.
   *
   * @return \Drupal\entity_reservation_system\Entity\ReservationSlotInterface
   *   The called Reservation slot entity.
   */
  public function setCode($code);

  /**
   * Gets the Reservation slot creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Reservation slot.
   */
  public function getCreatedTime();

  /**
   * Sets the Reservation slot creation timestamp.
   *
   * @param int $timestamp
   *   The Reservation slot creation timestamp.
   *
   * @return \Drupal\entity_reservation_system\Entity\ReservationSlotInterface
   *   The called Reservation slot entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Reservation slot published status indicator.
   *
   * Unpublished Reservation slot are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Reservation slot is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Reservation slot.
   *
   * @param bool $published
   *   TRUE to set this Reservation slot to published, FALSE otherwise.
   *
   * @return \Drupal\entity_reservation_system\Entity\ReservationSlotInterface
   *   The called Reservation slot entity.
   */
  public function setPublished($published);

  /**
   * Get the currente reservation status.
   *
   * @return string
   *   The status of the reservation.
   */
  public function getReservationStatus();

  /**
   * Set the reservation status.
   *
   * @param string $status
   *   The new status of the reservation.
   */
  public function setReservationStatus($status);

  /**
   * Set the expiration date of a reservation.
   *
   * @return int
   *   The expiration timestamp.
   */
  public function getReservationExpire();

  /**
   * Set the expiration of a reservation.
   *
   * @param int $expire
   *   The expiration timestamp.
   */
  public function setReservationExpire($expire);

  /**
   * Check if a reservation is expired.
   *
   * @return bool
   *   True if the reservation is expired, false otherwise.
   */
  public function checkExpire();

  /**
   * Gets the Reservation slot revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Reservation slot revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\entity_reservation_system\Entity\ReservationSlotInterface
   *   The called Reservation slot entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Reservation slot revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Reservation slot revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\entity_reservation_system\Entity\ReservationSlotInterface
   *   The called Reservation slot entity.
   */
  public function setRevisionUserId($uid);

  /**
   * Sets the Reservation slot revision author.
   */
  public function getBaseConfig();

}
