<?php

namespace Drupal\entity_reservation_system;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;

/**
 * Provides dynamic permissions.
 */
class EntityReservationSystemPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Constructs a ContentTranslationPermissions instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * Returns an array of content translation permissions.
   *
   * @return array
   *   The list of generated permissions.
   */
  public function generatePermissions() {
    $permission = [];
    // Create dynamic permissions to manage units per entity type and bundle.
    // Create tabs for all possible entity types.
    $fields = $this->entityFieldManager->getFieldMapByFieldType('reservation_slot');
    foreach ($fields as $entity_type_id => $field) {
      foreach (reset($field)['bundles'] as $bundle) {
        $entity_type_definition = $this->entityTypeManager->getDefinition($entity_type_id);
        $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
        $t_args = [
          '%entity_label' => $entity_type_definition->getLabel(),
          '%bundle' => $bundles[$bundle]['label'],
        ];
        $permission["manage units for $entity_type_id $bundle"] = [
          'title' => $this->t('Manage units for %entity_label of bundle %bundle', $t_args),
        ];
        $permission["manage reservations for $entity_type_id $bundle"] = [
          'title' => $this->t('Manage reservations for %entity_label of bundle %bundle', $t_args),
        ];
      }
    }
    return $permission;
  }

}
