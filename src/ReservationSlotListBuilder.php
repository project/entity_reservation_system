<?php

namespace Drupal\entity_reservation_system;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Defines a class to build a listing of Reservation slot entities.
 *
 * @ingroup entity_reservation_system
 */
class ReservationSlotListBuilder extends EntityListBuilder implements FormInterface {

  use RedirectDestinationTrait;

  /**
   * A instance of entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  protected $limit = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $hostEntityType = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $hostEntityId = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $hostFieldName = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $hostDelta = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $uid = FALSE;

  /**
   * Constructs a new BlockListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    FormBuilderInterface $form_builder,
    Request $request,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($entity_type, $storage);
    $this->formBuilder = $form_builder;
    $this->request = $request;
    $this->routeMatch = $route_match;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager')->getStorage($entity_type->id()),
      $container->get('form_builder'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reservation_slot_list_builder';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['filters'] = [
      '#type' => 'container',
      '#weight' => 1,
      '#attributes' => [
        'class' => ['container-inline'],
      ],
    ];
    $reservation_status = $this->request->query->get('reservation_status');
    $form['filters']['reservation_status'] = [
      '#type' => 'select',
      '#default_value' => $reservation_status,
      '#options' => [
        '' => $this->t('- All -'),
        ReservationsInterface::RESERVATION_STATUS_LOCKED => $this->t(ReservationsInterface::RESERVATION_STATUS_LOCKED_LABEL),
        ReservationsInterface::RESERVATION_STATUS_PENDING => $this->t(ReservationsInterface::RESERVATION_STATUS_PENDING_LABEL),
        ReservationsInterface::RESERVATION_STATUS_CONFIRMED => $this->t(ReservationsInterface::RESERVATION_STATUS_CONFIRMED_LABEL),
        ReservationsInterface::RESERVATION_STATUS_CANCELLED => $this->t(ReservationsInterface::RESERVATION_STATUS_CANCELLED_LABEL),
      ],
      '#title' => $this->t('Status'),
    ];
    $date = $this->request->query->get('date');
    $form['filters']['date'] = [
      '#type' => 'date',
      '#title' => $this->t('Date created from'),
      '#title_display' => 'before',
      '#date_format' => 'Y-m-d',
      '#default_value' => $date,
    ];
    $code = $this->request->query->get('code');
    $form['filters']['code'] = [
      '#type' => 'textfield',
      '#default_value' => $code,
      '#title' => $this->t('Code'),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Filter'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();
    if (!empty($this->hostEntityType)) {
      $hostEntity = $this->routeMatch->getParameter($this->hostEntityType);
      $url = Url::fromRoute($this->routeMatch->getRouteName(), [$this->hostEntityType => $hostEntity->id()], ['query' => $form_state->getValues()]);
      $form_state->setRedirectUrl($url);
    }
    else {
      $form_state->setRedirect($this->routeMatch->getRouteName(), $form_state->getValues());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entity_query = $this->storage->getQuery();
    $entity_query->condition('reservation_status', [
      ReservationsInterface::RESERVATION_STATUS_EXPIRED,
    ], 'NOT IN');
    $reservation_status = $this->request->query->get('reservation_status');
    if (!empty($reservation_status)) {
      $entity_query->condition('reservation_status', $reservation_status);
    }
    $date = $this->request->query->get('date');
    if (!empty($date)) {
      $entity_query->condition('reserved_dates', $date . 'T%', 'LIKE');
    }
    $code = $this->request->query->get('code');
    if (!empty($code)) {
      $entity_query->condition('code', '%' . Database::getConnection()->escapeLike($code) . '%', 'LIKE');
    }
    if (!empty($this->hostEntityType)) {
      $entity_query->condition('entity_type', $this->hostEntityType);
    }
    if (!empty($this->hostEntityId)) {
      $entity_query->condition('entity_id', $this->hostEntityId);
    }
    if (!empty($this->hostFieldName)) {
      $entity_query->condition('field_name', $this->hostFieldName);
    }
    if (!empty($this->hostDelta)) {
      $entity_query->condition('delta', $this->hostDelta);
    }
    if (!empty($this->uid)) {
      $entity_query->condition('uid', $this->uid);
    }
    $header = $this->buildHeader();
    $entity_query->pager($this->limit);
    $entity_query->tableSort($header);
    $uids = $entity_query->execute();
    return $this->storage->loadMultiple($uids);
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = [
      'data' => $this->t('Id'),
      'field' => 'id',
      'specifier' => 'id',
      'sort' => 'DESC',
    ];
    $header['code'] = [
      'data' => $this->t('Code'),
      'field' => 'code',
      'specifier' => 'code',
      'sort' => 'ASC',
    ];
    $header['reservation_status'] = [
      'data' => $this->t('Status'),
      'field' => 'reservation_status',
      'specifier' => 'reservation_status',
      'sort' => 'ASC',
    ];
    $header['date'] = [
      'data' => $this->t('Date'),
      'field' => 'date',
      'sort' => 'DESC',
      'specifier' => 'date',
    ];
    $header['changed'] = [
      'data' => $this->t('Changed'),
      'field' => 'changed',
      'sort' => 'DESC',
      'specifier' => 'changed',
    ];
    $header['operations'] = [
      'data' => $this->t('Operations'),
    ];
    $header = $header + parent::buildHeader();
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->id();
    if ($this->hostEntityType && $this->hostEntityId) {
      $hostEntity = $this->entityTypeManager->getStorage($entity->getHostEntityType())->load($entity->getHostEntityId());
      $row['code'] = Link::fromTextAndUrl(
        $entity->getCode(),
        $hostEntity->toUrl('drupal:reservation-slot-view')->setRouteParameter('reservation_slot', $entity->id())
      );
    }
    elseif ($this->uid) {
      $row['code'] = Link::fromTextAndUrl(
        $entity->getCode(),
        Url::fromRoute('entity_reservation_system.view_reservation', ['reservation_slot' => $entity->id()])
      );
    }
    else {
      $row['code'] = Link::fromTextAndUrl(
        $entity->getCode(),
        $entity->toUrl()
      );
    }
    $row['reservation_status'] = $this->t('@reservationlabel', ['@reservationlabel' => constant('Drupal\entity_reservation_system\ReservationsInterface::RESERVATION_STATUS_' . strtoupper($entity->getReservationStatus()) . '_LABEL')]);
    $row['date'] = $this->dateFormatter->format(strtotime($entity->reserved_dates->first()->value), 'reservation_date');
    $row['changed'] = $this->dateFormatter->format($entity->changed->value, 'short');
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   *
   * Builds the entity listing as renderable array for table.html.twig.
   *
   * @todo Add a link to add a new item to the #empty code.
   */
  public function render($limit = 50, $entity_type = NULL, $entity_id = NULL, $field_name = NULL, $delta = NULL, $uid = NULL) {
    $this->limit = $limit;
    $this->hostEntityType = $entity_type;
    $this->hostEntityId = $entity_id;
    $this->hostFieldName = $field_name;
    $this->hostDelta = $delta;
    $this->uid = $uid;

    $build['#title'] = $this->t('Reservations');

    $build['form'] = [
      $this->formBuilder()->getForm($this),
    ];

    $build['table'] = [
      '#type' => 'table',
      '#title' => $this->getTitle(),
      '#rows' => [],
      '#header' => $this->buildHeader(),
      '#empty' => $this->t('There is no @label yet.', ['@label' => $this->entityType->getLabel()]),
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
    ];
    foreach ($this->load() as $entity) {
      if ($row = $this->buildRow($entity)) {
        $build['table']['#rows'][$entity->id()] = $row;
      }
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }
    return $build;
  }

  /**
   * Returns the form builder.
   *
   * @return \Drupal\Core\Form\FormBuilderInterface
   *   The form builder.
   */
  protected function formBuilder() {
    if (!$this->formBuilder) {
      $this->formBuilder = \Drupal::formBuilder();
    }
    return $this->formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = [];
    if ($this->hostEntityType && $this->hostEntityId) {
      // Prepare operations for admin users.
      $hostEntity = $this->entityTypeManager->getStorage($entity->getHostEntityType())->load($entity->getHostEntityId());
      if ($entity->access('update') && $hostEntity->hasLinkTemplate('drupal:reservation-slot-edit')) {
        $operations['edit'] = [
          'title' => $this->t('Edit'),
          'weight' => 10,
          'url' => $this->ensureDestination($hostEntity->toUrl('drupal:reservation-slot-edit')->setRouteParameter('reservation_slot', $entity->id())),
        ];
      }
      if ($entity->access('delete') && $hostEntity->hasLinkTemplate('drupal:reservation-slot-delete')) {
        $operations['delete'] = [
          'title' => $this->t('Delete'),
          'weight' => 100,
          'url' => $this->ensureDestination($hostEntity->toUrl('drupal:reservation-slot-delete')->setRouteParameter('reservation_slot', $entity->id())),
        ];
      }
      $operations += $this->moduleHandler()->invokeAll('entity_operation', [$entity]);
      $this->moduleHandler->alter('entity_operation', $operations, $entity);
      uasort($operations, '\Drupal\Component\Utility\SortArray::sortByWeightElement');
    }
    elseif ($this->uid) {
      $account = $this->entityTypeManager->getStorage('user')->load($this->uid);
      // Prepare operations for normal users.
      if ($account->hasPermission('edit own reservation slot')) {
        $operations['edit'] = [
          'title' => $this->t('Edit'),
          'weight' => 10,
          'url' => Url::fromRoute('entity_reservation_system.edit_reservation_form', ['reservation_slot' => $entity->id()]),
        ];
      }
      if ($account->hasPermission('cancel own reservation slot')) {
        $operations['cancel'] = [
          'title' => $this->t('Cancel'),
          'weight' => 100,
          'url' => Url::fromRoute('entity_reservation_system.cancel_reservation_form', ['reservation_slot' => $entity->id()]),
        ];
      }
    }
    else {
      $operations = parent::getDefaultOperations($entity);
    }
    return $operations;
  }

}
