<?php

namespace Drupal\entity_reservation_system;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_reservation_system\Plugin\SlotCondition\SlotConditionManager;

/**
 * Access check for entity translation overview.
 */
class TimeSlotsService {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The slot condition manager.
   *
   * @var \Drupal\entity_reservation_system\Plugin\SlotCondition\SlotConditionManager
   */
  protected $slotConditionManager;

  /**
   * Constructs a ContentTranslationOverviewAccess object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\entity_reservation_system\Plugin\SlotCondition\SlotConditionManager $slot_condition_manager
   *   The slot condition manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, SlotConditionManager $slot_condition_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->slotConditionManager = $slot_condition_manager;
  }

  /**
   * Checks access to the translation overview for the entity and bundle.
   */
  public function getTimeSlots($day, array $units, array $config, array $context, array $current = NULL) {
    // 24 hours -> 24 * 60 = 1440.
    $range = range(0, 1339, $config['config']['slot_size']);
    $slots = [];
    $daytime = strtotime($day);
    foreach ($range as $slot) {
      $slots[$slot] = date('H:i', $daytime + ($slot * 60)) . ' - ' . date('H:i', $daytime + (($slot + $config['config']['slot_size']) * 60));
    }
    $preservedSlots = [];
    if ($current) {
      $current = array_combine($current, $current);
      $preservedSlots = array_intersect_key($slots, $current);
    }
    // Allow filter all slots by conditions defined as plugins. Each plugin
    // can evaluate only slots not previously removed.
    $conditions = $this->slotConditionManager->loadAllDefinitions();
    foreach ($conditions as $condition) {
      $slots = $condition->filter($slots, $day, $units, $config, $context);
    }
    $slots = $slots + $preservedSlots;
    ksort($slots);
    return $slots;
  }

}
