<?php

namespace Drupal\entity_reservation_system;

/**
 * Provides an interface defining a Reservation entity.
 */
interface ReservationsInterface {

  /**
   * Reservation form  status open.
   */
  const STATUS_OPEN = 'open';

  /**
   * Reservation form  status private.
   */
  const STATUS_PRIVATE = 'private';

  /**
   * Reservation form  status closed.
   */
  const STATUS_CLOSED = 'closed';

  /**
   * Reservation form  status scheduled.
   */
  const STATUS_SCHEDULED = 'scheduled';

  /**
   * Reservation form status private scheduled.
   */
  const STATUS_PRIVATE_SCHEDULED = 'private_scheduled';

  /**
   * Reservation form  status open.
   */
  const STATUS_OPEN_LABEL = 'Open';

  /**
   * Reservation form  status private.
   */
  const STATUS_PRIVATE_LABEL = 'Private';

  /**
   * Reservation form  status closed.
   */
  const STATUS_CLOSED_LABEL = 'Closed';

  /**
   * Reservation form  status scheduled.
   */
  const STATUS_SCHEDULED_LABEL = 'Scheduled';

  /**
   * Reservation form status private scheduled.
   */
  const STATUS_PRIVATE_SCHEDULED_LABEL = 'Private Scheduled';

  /**
   * Reservation form status private scheduled.
   */
  const RESERVATION_STATUS_LOCKED = 'locked';

  /**
   * Reservation form status private scheduled.
   */
  const RESERVATION_STATUS_EXPIRED = 'expired';
  /**
   * Reservation form status private scheduled.
   */
  const RESERVATION_STATUS_PENDING = 'pending';
  /**
   * Reservation form status private scheduled.
   */
  const RESERVATION_STATUS_CONFIRMED = 'confirmed';
  /**
   * Reservation form status private scheduled.
   */
  const RESERVATION_STATUS_CANCELLED = 'cancelled';

  /**
   * Reservation form status private scheduled.
   */
  const RESERVATION_STATUS_LOCKED_LABEL = 'Locked';

  /**
   * Reservation form status private scheduled.
   */
  const RESERVATION_STATUS_EXPIRED_LABEL = 'Expired';
  /**
   * Reservation form status private scheduled.
   */
  const RESERVATION_STATUS_PENDING_LABEL = 'Pending';
  /**
   * Reservation form status private scheduled.
   */
  const RESERVATION_STATUS_CONFIRMED_LABEL = 'Confirmed';
  /**
   * Reservation form status private scheduled.
   */
  const RESERVATION_STATUS_CANCELLED_LABEL = 'Cancelled';

}
