<?php

namespace Drupal\entity_reservation_system\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_reservation_system\ReservationsInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Access check for entity translation overview.
 */
class ReservationUserAccess implements AccessInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a ContentTranslationOverviewAccess object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Checks access for distinct routes for reservation management.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The currently logged in account.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param string $op
   *   The operator to check.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(RouteMatchInterface $route_match, AccountInterface $account, $op) {
    if ($op == 'add') {
      $parameters = $route_match->getParameters();
      if (empty($parameters->get('entity_type')) || empty($parameters->get('entity_id')) || empty($parameters->get('field_name'))) {
        return AccessResult::neutral();
      }
      $host = $this->entityTypeManager->getStorage($parameters->get('entity_type'))->load($parameters->get('entity_id'));
      $field_name = $parameters->get('field_name');
      $config = $host->{$field_name}->first()->getPropertyValues();
      switch ($config['status']) {
        case ReservationsInterface::STATUS_CLOSED:
          // Nothing to do, if closed the status is forbidden.
          break;

        case ReservationsInterface::STATUS_OPEN:
          if ($account->hasPermission('add reservation slot')) {
            return AccessResult::allowed();
          }
          break;

        case ReservationsInterface::STATUS_PRIVATE:
          if ($account->hasPermission('add private reservation slot')) {
            return AccessResult::allowed();
          }
          break;

        case ReservationsInterface::STATUS_SCHEDULED:
          if ($account->hasPermission('add reservation slot')) {
            $currentDate = date(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
            if ($currentDate >= $config['open'] &&  $currentDate <= $config['close']) {
              return AccessResult::allowed();
            }
          }
          break;

        case ReservationsInterface::STATUS_PRIVATE_SCHEDULED:
          if ($account->hasPermission('add private reservation slot')) {
            $currentDate = date(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
            if ($currentDate >= $config['open'] &&  $currentDate <= $config['close']) {
              return AccessResult::allowed();
            }
          }
          break;

      }
      return AccessResult::forbidden();
    }
    else {
      $reservation = $route_match->getParameter('reservation_slot');
      if ($reservation) {
        // Get entity base info.
        $entity_type_id = $reservation->getEntityTypeId();
        $bundle = $reservation->bundle();
        // Check per entity permission.
        $permission = "manage reservations for {$entity_type_id} {$bundle}";
        if ($account->hasPermission($permission)) {
          return AccessResult::allowed();
        }
        elseif ($account->hasPermission($op . ' own reservation slot') && $reservation->getOwnerId() == $account->id()) {
          return AccessResult::allowed();
        }
        else {
          return AccessResult::forbidden();
        }
      }
      else {
        return AccessResult::forbidden();
      }
    }

    // Forbidden anyway.
    return AccessResult::forbidden();
  }

}
