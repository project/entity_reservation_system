<?php

namespace Drupal\entity_reservation_system;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\entity_reservation_system\Entity\ReservationSlotInterface;

/**
 * Defines the storage handler class for Reservation slot entities.
 *
 * This extends the base storage class, adding required special handling for
 * Reservation slot entities.
 *
 * @ingroup entity_reservation_system
 */
class ReservationSlotStorage extends SqlContentEntityStorage implements ReservationSlotStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(ReservationSlotInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {reservation_slot_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {reservation_slot_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(ReservationSlotInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {reservation_slot_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('reservation_slot_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
