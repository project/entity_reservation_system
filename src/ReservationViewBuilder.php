<?php

namespace Drupal\entity_reservation_system;

use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Theme\Registry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * View builder handler for nodes.
 */
class ReservationViewBuilder extends EntityViewBuilder {

  /**
   * A instance of entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Variable that stores the user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Constructs a new EntityViewBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Theme\Registry $theme_registry
   *   The theme registry.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The account variable.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityRepositoryInterface $entity_repository,
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entity_type_manager,
    Registry $theme_registry = NULL,
    EntityDisplayRepositoryInterface $entity_display_repository = NULL,
    AccountProxyInterface $account
  ) {
    parent::__construct($entity_type, $entity_repository, $language_manager, $theme_registry, $entity_display_repository);
    $this->entityTypeManager = $entity_type_manager;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $container->get('theme.registry'),
      $container->get('entity_display.repository'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {
    if (empty($entities)) {
      return;
    }
    parent::buildComponents($build, $entities, $displays, $view_mode);
    foreach ($entities as $id => $entity) {
      $bundle = $entity->bundle();
      $display = $displays[$bundle];
      $hostEntity = $this->entityTypeManager->getStorage($entity->getHostEntityType())->load($entity->getHostEntityId());
      if ($display->getComponent('host_entity') && $hostEntity->access('view', $this->account)) {
        $build[$id]['host_entity'] = [
          '#theme' => 'reservation_extra_field',
          '#attributes' => [],
          '#title_attributes' => [],
          '#field_name' => 'host_entity',
          '#field_type' => 'link',
          '#label_display' => 'above',
          '#label_hidden' => FALSE,
          '#label' => $this->t('Reserved item'),
          '#multiple' => FALSE,
          '#items' => [],
        ];
        $build[$id]['host_entity']['#items'][]['content'] = [
          '#type' => 'link',
          '#title' => $hostEntity->label(),
          '#url' => $hostEntity->toUrl(),
        ];
      }
    }
  }

}
